<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\UserRetriever;
use App\Http\Middleware\DashboardReciever;
use App\Http\Middleware\Authenticator;
use App\Http\Middleware\VoteRetriever;
use App\Http\Middleware\ElectionReciever;
use App\Http\Middleware\TimeValidator;
use App\Http\Middleware\ElectionVisitsCounter;

Route::get('/', function () {	
    return view('index');
})->name("index")->middleware(UserRetriever::class);

Route::get('/login', function()
{
	return view('login', ["error" => false]);
})->name("login")->middleware(UserRetriever::class);

Route::get('/create', function()
{
	return view('create', ["error" => false]);
})->name("signup")->middleware(UserRetriever::class);


Route::get('/gogreen', function()
{
	return view('gogreen');
})->middleware(UserRetriever::class);

Route::get("/terms", function()
{
	return view("terms");
})->middleware(UserRetriever::class);

Route::get("/privacy-policy", function()
{
	return view("privacy");
})->middleware(UserRetriever::class);


// Dashboard Routes and controllers
Route::get('/dashboard', "DashboardController@open")->name("dashboard")->middleware(Authenticator::class)->middleware(UserRetriever::class)->middleware(DashboardReciever::class);

Route::get('/dashboard/tabs/{tab}', function($tab)
{
	return view('dashboard.tabs.' . $tab); // Render the tab
})->middleware(Authenticator::class)->middleware(UserRetriever::class)->middleware(DashboardReciever::class);

Route::get("/dashboard/election/{id}", "DashboardController@election")->middleware(Authenticator::class)->middleware(UserRetriever::class)->middleware(DashboardReciever::class);

Route::get("/dashboard/election/{id}/{tab}", "DashboardController@tab")->middleware(Authenticator::class)->middleware(UserRetriever::class)->middleware(DashboardReciever::class);

// Facebook Login routes
Route::get("/user/login/facebook", 'LoginController@redirectToFacebook');
Route::get('/user/login/facebook/callback', 'LoginController@handleFacebookCallback');

// Google Plus Login routes
Route::get("/user/login/google", 'LoginController@redirectToGooglePlus');
Route::get('/user/login/google/callback', 'LoginController@handleGooglePlusCallback');

// Email login routes
Route::post("/user/create", "UserController@create");
Route::post("/user/login", "UserController@login");

// Data Management routes and functions
Route::get("/data/election/{id}", "DataController@election");
Route::post("/data/election/{id}", "SaveController@election");

// Image Management routes and functions
// Upload an image to the server and return the URL of the image
Route::post("/image", "ImageController@uploadImage");
Route::get("/image/{id}", "ImageController@downloadImage");

// Voting Routes
Route::get("/vote/{id}", "VoteController@vote")
	->middleware(Authenticator::class) // The user must be logged in to access it
	->middleware(UserRetriever::class)
	->middleware(VoteRetriever::class)
	->middleware(TimeValidator::class) // Check if the time is valid
	->middleware(ElectionReciever::class) // Get the election variables
	->middleware(ElectionVisitsCounter::class); // Counts visits for the election in the database

// Voting function
Route::get("/election/vote/{id}/{vote}", "VoteController@submitVote")
	->middleware(Authenticator::class)
	->middleware(UserRetriever::class);

// Thank you route
Route::get("/election/vote/thanks", function()
	{
		return view("vote.thanks");
	})->name("thankyou")
	->middleware(Authenticator::class) // The user must be logged in to access it
	->middleware(UserRetriever::class); // The view recieves election information;

// Preview Voting Sequences
Route::get("/preview/{electionid}", "PreviewController@Preview")
	->middleware(Authenticator::class) // The user must be logged in to access it
	->middleware(UserRetriever::class) // Retrieve user information
	->middleware(DashboardReciever::class); // Recieve dashboard information

// Election Management Routes
Route::post("/user/election/create", "ElectionController@create");

Route::post("/feedback", "FeedbackController@save");

// Settings and Setting Functions
Route::post("/settings/save/organization", "SettingsController@saveOrganization");
Route::post("/settings/save/user", "SettingsController@saveUser");
Route::post("/settings/save/appearance", "SettingsController@saveAppearance");
Route::post("/settings/save/election/details/{id}", "SettingsController@electionDetails");
Route::post("/settings/save/election/time/{id}", "SettingsController@electionTiming");

// Administrative Functions
Route::get("/settings/election/launch/{id}", "SettingsController@launchElection");
Route::get("/settings/election/abort/{id}", "SettingsController@abortElection");
Route::get("/settings/election/delete/{id}", "SettingsController@deleteElection");

Route::get("/signout", "UserController@signOut");