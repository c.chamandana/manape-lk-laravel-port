@charset "UTF-8";
* {
  font-family: 'Poppins', sans-serif; }

body {
  background-color: white;
  overflow-x: hidden;
  width: 100vw !important; }

html {
  width: 100vw !important;
  overflow-x: hidden; }

.arrows {
  width: 60px;
  height: 72px;
  position: absolute;
  left: 50%;
  margin-left: -30px;
  bottom: 150px;
  transform: scale(0.5); }

@media only screen and (min-height: 600px) {
  .arrows {
    display: none; } }
.arrows path {
  stroke: #216ddd;
  fill: transparent;
  stroke-width: 4px;
  animation: arrow 2s infinite;
  -webkit-animation: arrow 2s infinite; }

@keyframes arrow {
  0% {
    opacity: 0; }
  40% {
    opacity: 1; }
  80% {
    opacity: 0; }
  100% {
    opacity: 0; } }
@-webkit-keyframes arrow /*Safari and Chrome*/ {
  0% {
    opacity: 0; }
  40% {
    opacity: 1; }
  80% {
    opacity: 0; }
  100% {
    opacity: 0; } }
.arrows path.a1 {
  animation-delay: -1s;
  -webkit-animation-delay: -1s;
  /* Safari σÆî Chrome */ }

.arrows path.a2 {
  animation-delay: -0.5s;
  -webkit-animation-delay: -0.5s;
  /* Safari σÆî Chrome */ }

.arrows path.a3 {
  animation-delay: 0s;
  -webkit-animation-delay: 0s;
  /* Safari σÆî Chrome */ }

.header {
  padding-top: 50px;
  padding-bottom: 75px; }
  .header .mobile-menu-button {
    display: none; }
  .header .mobile-menu-close-button {
    display: none; }
  .header .brand {
    display: inline; }
    .header .brand .brand-logo {
      height: 2.5em; }
    .header .brand .brand-text {
      font-weight: 900;
      margin-left: 10px;
      font-size: 2em;
      position: relative;
      bottom: 10px;
      margin-bottom: 26px;
      display: inline-block;
      color: #216ddd; }
  .header .menu {
    float: right;
    font-weight: bold;
    color: #555;
    text-transform: uppercase;
    font-size: 0.8em;
    padding-top: 10px; }
    .header .menu .regular-menu {
      display: inline; }
      .header .menu .regular-menu li {
        color: #555 !important;
        display: inline;
        letter-spacing: 2px;
        margin-right: 40px;
        font-family: 'Poppins', sans-serif;
        transition: opacity 500ms; }
      .header .menu .regular-menu li:hover {
        opacity: 0.7; }
    .header .menu .account-menu {
      display: inline; }
      .header .menu .account-menu li {
        display: inline;
        margin-right: 25px;
        letter-spacing: 2px;
        font-family: 'Poppins', sans-serif; }
      .header .menu .account-menu .login-button {
        margin-left: 25px;
        color: #216ddd;
        transition: opacity 500ms; }
      .header .menu .account-menu .login-button:hover {
        opacity: 0.7; }
      .header .menu .account-menu .signup-button {
        background-color: #216ddd;
        color: white;
        padding: 10px 15px;
        border-radius: 50px;
        transition: all 500ms;
        cursor: pointer; }
      .header .menu .account-menu .signup-button:hover {
        opacity: 0.7; }

@media only screen and (max-width: 600px) {
  .header {
    padding-top: 25px;
    padding-bottom: 25px; }
    .header .container {
      text-align: center; }
    .header .mobile-menu-button {
      display: initial;
      float: left;
      color: #216ddd;
      cursor: pointer; }
    .header .menu {
      pointer-events: none;
      display: initial;
      width: 100%;
      height: 100vh;
      transform: scale(1);
      opacity: 1;
      background-color: rgba(255, 255, 255, 0.9);
      position: fixed;
      top: 0;
      left: 0;
      z-index: 9000;
      transform: translateX(-100%);
      padding-left: 75px;
      padding-right: 75px;
      padding-top: 50%;
      transition: transform 500ms, opacity 500ms; }
      .header .menu .mobile-menu-close-button {
        display: block;
        color: #216ddd;
        position: fixed;
        top: 45px;
        right: 45px; }
      .header .menu .regular-menu {
        display: block; }
        .header .menu .regular-menu li {
          display: block;
          margin-bottom: 35px;
          color: #555 !important;
          letter-spacing: 2px;
          margin-right: 0 !important;
          font-family: 'Poppins', sans-serif;
          transition: opacity 500ms; }
      .header .menu .account-menu {
        display: block; }
        .header .menu .account-menu li {
          margin-bottom: 35px;
          margin-left: 0 !important;
          display: block;
          margin-right: 0 !important; }
    .header .menu.shown {
      pointer-events: all;
      transform: translateX(0); }

  .arrows {
    display: none; } }
.blue-background {
  background-color: #1a7bff;
  color: white;
  padding-bottom: 50px; }
  .blue-background .steps {
    margin-top: 25px; }
    .blue-background .steps td {
      padding-top: 0 !important;
      padding-bottom: 25px !important; }
    .blue-background .steps .fa {
      margin-right: 15px; }
  .blue-background .feature-title {
    font-size: 1.5em;
    text-transform: uppercase;
    display: block;
    margin-bottom: 5px; }
  .blue-background .feature-subtitle {
    font-size: 0.8em;
    font-family: 'Poppins', sans-serif;
    opacity: 0.7;
    display: block; }
  .blue-background .step-title {
    font-weight: bold;
    margin-bottom: 5px; }
  .blue-background .step-description {
    font-family: 'Poppins', sans-serif;
    font-size: 11px;
    opacity: 0.8; }

.feature {
  margin-bottom: 100px !important; }
  .feature .main-feature-text {
    color: #216ddd;
    font-size: 30px;
    font-weight: lighter;
    margin-bottom: 0; }
  .feature .feature-subtitle-text {
    font-size: 11px;
    font-weight: bolder;
    color: #555;
    font-family: 'Poppins', sans-serif;
    margin-bottom: 25px; }
  .feature .feature-checkout-button {
    color: #216ddd;
    border: 2px solid #216ddd;
    border-radius: 50px;
    background-color: white;
    padding: 5px 20px;
    font-size: 12px;
    font-family: 'Poppins', sans-serif;
    transition: all 250ms; }
  .feature .feature-checkout-button:hover {
    background-color: #216ddd;
    color: white; }
  .feature .col.feature-image {
    text-align: right; }
    .feature .col.feature-image img {
      width: 100%; }
  .feature .feature-content {
    margin-top: 40px; }
  .feature .feature-item td {
    padding-top: 0 !important;
    padding-bottom: 0 !important; }
  .feature .feature-item .feature-item-image {
    width: 75px;
    display: inline;
    margin-right: 10px; }
  .feature .feature-item .feature-title {
    display: block;
    color: #216ddd;
    font-weight: bolder;
    text-transform: uppercase;
    font-size: 12px; }
  .feature .feature-item .feature-description {
    display: block;
    font-weight: bold;
    font-size: 11px;
    font-family: 'Poppins', sans-serif;
    color: #666; }

.go-green-background {
  padding-top: 60px;
  padding-bottom: 60px;
  background-size: 55%;
  background-repeat: no-repeat;
  background-position: right;
  text-align: center; }

.go-green-image {
  height: 250px; }

.go-green-content {
  text-align: center; }

.go-green-topic {
  font-size: 2em;
  color: #216ddd;
  font-weight: bold; }

.gone-green-text {
  color: green; }

@media only screen and (max-width: 600px) {
  .feature {
    text-align: center; }

  .mobile-col-space {
    margin-bottom: 50px; }

  .go-green-background {
    background: white;
    background-image: none; } }
.blue-button {
  background-color: #216ddd;
  color: white;
  padding: 10px 15px;
  border-radius: 50px;
  font-weight: bold;
  font-size: 12px;
  border: none;
  transition: all 500ms; }

.blue-button.big {
  font-size: 15px;
  padding: 10px 25px; }

.blue-button:hover {
  opacity: 0.7; }

.blue-button:active {
  opacity: 1;
  background-color: #216ddd; }

.blue-button:focus {
  opacity: 1;
  background-color: #216ddd; }

/* label underline focus color */
input:focus {
  border-bottom: 1px solid #216ddd !important;
  box-shadow: 0 1px 0 0 #216ddd !important; }

input:focus + label {
  color: #216ddd !important; }

textarea:focus {
  border-bottom: 1px solid #216ddd !important;
  box-shadow: 0 1px 0 0 #216ddd !important; }

textarea:focus + label {
  color: #216ddd !important; }

.input-field {
  padding-left: 10px;
  padding-right: 10px; }

.contact-form {
  padding-top: 100px;
  padding-bottom: 100px; }

.gray-topic {
  color: #666;
  font-size: 2em;
  text-transform: uppercase;
  display: block; }

.blue-subtext {
  color: #216ddd;
  font-size: 15px;
  display: block;
  font-weight: bold;
  margin-bottom: 5px; }

.gray-subtext {
  color: #555;
  font-size: 15px;
  display: block;
  font-weight: bold; }

.large-space {
  margin-bottom: 80px; }

.space {
  margin-bottom: 40px; }

.map-background {
  background-color: white;
  background-size: 100%; }

@media only screen and (max-width: 600px) {
  .contact-form {
    padding-top: 50px;
    padding-bottom: 50px; }
    .contact-form .gray-topic {
      text-align: center; }
    .contact-form .blue-subtext {
      text-align: center; }
    .contact-form .gray-subtext {
      text-align: center; } }
.testimonial {
  height: 400px !important;
  text-align: center; }
  .testimonial .background-dropper {
    width: 100%;
    height: 100%;
    background: linear-gradient(to-right, white, rgba(255, 255, 255, 0), white); }

figure.snip1192 {
  font-family: 'Raleway', Arial, sans-serif;
  position: relative;
  overflow: hidden;
  margin: 10px 1%;
  min-width: 220px;
  max-width: 310px;
  width: 100%;
  color: #333;
  left: 50%;
  transform: translateX(-50%);
  text-align: center;
  box-shadow: none !important; }

figure.snip1192 * {
  -webkit-box-sizing: border-box;
  box-sizing: border-box; }

figure.snip1192 img {
  max-width: 100%;
  height: 100px;
  width: 100px;
  border-radius: 50%;
  margin-bottom: 15px;
  display: inline-block;
  z-index: 1;
  position: relative; }

figure.snip1192 blockquote {
  margin: 0;
  display: block;
  border-radius: 8px;
  position: relative;
  background-color: #fafafa;
  border-left: none;
  padding: 30px 50px 65px 50px;
  font-size: 0.8em;
  font-weight: 500;
  margin: 0 0 -50px;
  line-height: 1.6em;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15); }

figure.snip1192 blockquote:before,
figure.snip1192 blockquote:after {
  font-family: 'FontAwesome';
  content: "\201C";
  position: absolute;
  font-size: 50px;
  opacity: 0.3;
  color: #216ddd;
  font-style: normal; }

figure.snip1192 blockquote:before {
  top: 35px;
  left: 20px; }

figure.snip1192 blockquote:after {
  content: "\201D";
  right: 20px;
  bottom: 35px; }

figure.snip1192 .author {
  margin: 0;
  text-transform: uppercase;
  text-align: center;
  color: #ffffff; }

figure.snip1192 .author h5 {
  opacity: 1;
  margin: 0;
  font-weight: 800;
  letter-spacing: 1px;
  font-size: 15px; }

figure.snip1192 .author h5 span {
  font-weight: 400;
  text-transform: none;
  margin-top: 5px;
  letter-spacing: 1px;
  display: block;
  font-size: 13px; }

.footer .upperFooter {
  background-color: #216ddd;
  color: white !important;
  padding-top: 40px;
  padding-bottom: 15px; }
  .footer .upperFooter .brand-playstore-link {
    height: 75px;
    transform: translateY(-15px);
    float: right; }
  .footer .upperFooter .brand-logo {
    height: 30px;
    margin-bottom: 25px; }
  .footer .upperFooter .center {
    text-align: center; }
  .footer .upperFooter .non-center {
    display: inline-block;
    text-align: left; }
  .footer .upperFooter .right {
    text-align: right; }
  .footer .upperFooter a {
    color: rgba(255, 255, 255, 0.6) !important; }
  .footer .upperFooter a:hover {
    color: white; }
  .footer .upperFooter a:visited {
    color: white; }
  .footer .upperFooter a:focus {
    color: white; }
.footer .copyrights {
  background-color: #164d9e;
  color: white !important;
  padding-top: 15px;
  padding-bottom: 15px; }
  .footer .copyrights .copyrights-left {
    display: inline; }
    .footer .copyrights .copyrights-left span {
      font-size: 12px;
      font-weight: bold;
      opacity: 0.8; }
    .footer .copyrights .copyrights-left span:last-child {
      font-weight: normal;
      font-size: 11px;
      opacity: 0.6; }
  .footer .copyrights .copyrights-right {
    float: right;
    font-size: 11px; }
    .footer .copyrights .copyrights-right a {
      color: white !important;
      padding-right: 15px;
      border-right: solid 2px white;
      margin-right: 15px; }
    .footer .copyrights .copyrights-right a:last-child {
      margin-right: 0 !important;
      padding-right: 0 !important;
      border-right: none !important; }
    .footer .copyrights .copyrights-right a:hover {
      color: white; }
    .footer .copyrights .copyrights-right a:visited {
      color: white; }
    .footer .copyrights .copyrights-right a:focus {
      color: white; }

@media only screen and (max-width: 600px) {
  .footer .upperFooter .center {
    text-align: center; }
  .footer .upperFooter .right {
    text-align: center; }
  .footer .upperFooter .non-center {
    text-align: center; }
  .footer .container {
    text-align: center; }
  .footer .copyrights-right {
    margin-top: 15px;
    display: block !important;
    float: none !important;
    text-align: center; } }
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  background-color: white;
  width: 100%;
  height: 100vh;
  z-index: 10000; }

.preloader-wrapper-container {
  position: relative;
  top: 50%;
  text-align: center;
  left: 50%;
  transform: translateX(-50%) translateY(-50%); }

.circle-clipper .circle {
  border-width: 5px !important; }

.create-account {
  font-size: 13px;
  margin-left: 15px; }

.forgot-password {
  font-size: 13px;
  float: right;
  margin-top: 9px; }

/*# sourceMappingURL=stylesheet.cs.map */
