var $preloader = $(".preloader");

var opacitizePreloader = () =>
{
	$preloader.css("background-color", "rgba(255,255,255,0.9)");
};

var disablePreloader = () =>
{
	$preloader.fadeOut();
};

setTimeout(opacitizePreloader, 1000);
setTimeout(disablePreloader, 2000);

var hideMenu = () =>
{
	if ($navbar_menu.hasClass("shown"))
		$navbar_menu.removeClass("shown");
}

var showMenu = () => 
{
	if (!$navbar_menu.hasClass("shown"))
		$navbar_menu.addClass("shown");
}

var $mobile_menu_button = $(".mobile-menu-button");
var $mobile_menu_close_button = $(".mobile-menu-close-button");
var $navbar_menu = $("#navbar-menu");

$mobile_menu_button.click(function() 
{
	showMenu();
});

$mobile_menu_close_button.click(function()
{
	hideMenu();
});


$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});	