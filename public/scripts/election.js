var router = ccrouter();  // Create a new router from ccrouter

router.initialize(window.location.pathname + "/", "", {
    color:  "dodgerblue",
    height: "2",
    initial: "overview"
}, null, refreshPages); 

// Map the change of hashes to the router
router.listen(); // Listen for the incoming hashchanges

var $notifications_button = $("#notifications-button");
var $notifications = $(".notifications");

$(document).ready(function(){
	$notifications_button.click(function() 
	{
		$notifications.toggleClass("show"); 
	});	
});

function refreshPages($route)
{
	if ($route.includes("settings"))
	{
		$("#startdate").val(start.format(backFormat));
		$("#enddate").val(end.format(backFormat));
		$("#starttime").val(start.format(backFormatTime));
		$("#endtime").val(end.format(backFormatTime));

		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15, // Creates a dropdown of 15 years to control year,
			today: 'Today',
			clear: 'Clear',
			close: 'Ok',
			closeOnSelect: false // Close upon selecting a date,
		});

		$('.timepicker').pickatime({
			default: 'now', // Set default time: 'now', '1:30AM', '16:30'
			fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
			twelvehour: false, // Use AM/PM or 24-hour format
			donetext: 'OK', // text for done-button
			cleartext: 'Clear', // text for clear-button
			canceltext: 'Cancel', // Text for cancel-button
			autoclose: false, // automatic close timepicker
			ampmclickable: true, // make AM PM clickable
			aftershow: function(){} //Function for after opening timepicker
		});

		var editor = new Quill('#editor', {
			toolbar : { container: '#toolbar' },
			theme: 'snow'
		});

		var $editor = $("#editor .ql-editor");
		$editor.html(_description.replaceAll("&lt;", "<").replaceAll("&gt;", ">")); // Set the description

		$('input#election_title, textarea#election_description').characterCounter();
		Materialize.updateTextFields();

		$("#delete-confirm").modal();

		$("#delete-election-button").click(function(event)
		{
			$("#delete-confirm").modal("open");	
			event.preventDefault();
		});

		// Form controllers
		$("#save-election-timing-button").click(function(event)
		{
			var fdata = new FormData(document.getElementById("election_dates"));

			$.ajax(
			{
				headers : {
					"X-CSRF-TOKEN" : $('meta[name="csrf-token"]').attr('content')
				},
				url : "/settings/save/election/time/" + __id,
				type : "POST",
				data : fdata,
				contentType : false,
				processData : false,

				success : function(result) 
				{
					if (result == "SAVED")
					{
						Materialize.toast('Your settings are saved', 4000, "alert-information");
					}else if (result == "INPUT ERROR")
					{
						Materialize.toast('Please make sure the input is valid', 4000, "alert-error");
					}else if (result == "TIME ERROR")
					{
						Materialize.toast('End date and time must be after start date and time', 4000, "alert-error");
					}
				},
				error : function(er)
				{
					console.log(er);
				}
			});

			event.preventDefault();
		});

		$("#election-settings-save-btn").click(function(event) {
			
			var title = $("#election_title").val();
			var description = ($editor.html());

			var fdata = new FormData;
			fdata.append("title", title);
			fdata.append("description", description);

			// Send the AJAX request
			$.ajax(
			{
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '/settings/save/election/details/' + __id,
				type: 'POST',
				data: fdata,
				contentType: false,
				processData: false,

				success:function(response) {
					if (response == "SAVED")
					{
						Materialize.toast('Your settings are saved', 4000, "alert-information");
					} else {
						console.log(response);
					}
				}
		     });
			event.preventDefault();
		});
	}

	if ($route.includes("overview"))
	{
		if ($('#myChart').length > 0) {
			var ctx = document.getElementById("myChart").getContext('2d');
			ctx.height = 400;

			var myChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ["January", "February", "March", "April", "May", "June"],
			        datasets: [{
			            label: '# of Votes',
			            data: [531,1246,3414,2421,2313,5124],
			            backgroundColor: [
			                '#216ddd3f'
			            ],
			            borderColor: [
			                '#216dddff'
			            ],
			            borderWidth: 1,
			            lineTension: 0
			        }]
			    },
			    options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }]
			        }
			    }
			});
		}

		if ($('#votes_days').length > 0) {
			// If analytics are available
			var ctx = document.getElementById("votes_days").getContext('2d');
			ctx.height = 400;

			var myChart = new Chart(ctx, {
			   type : 'line', 
				data : {
					datasets : [{
						label : "# of Votes",
						data : _dayvotes,
						backgroundColor: [
			                '#216ddd3f'
			            ],
			            borderColor: [
			                '#216dddff'
			            ],
			            lineTension: 0
					}],

					labels: _daynumber
				},
				options : {
					scales: {
					 yAxes: [{
					     ticks: {
					         beginAtZero: true,
					         userCallback: function(label, index, labels) {
					             // when the floored value is the same as the value we have a whole number
					             if (Math.floor(label) === label) {
					                 return label;
					             }

					         },
					     }
					 }],
					},
				}
			});
		}
	}

	if ($route.includes("analytics"))
	{
		var votesCanvas = document.getElementById("votes-chart").getContext('2d');

		// Initialize the votes piechart
		var votesPieChart = new Chart(votesCanvas,{
		    type: 'pie',
			data : {
				datasets: [{
					label : "# of Votes",
				    data: _votesCount,
				    backgroundColor : _backgroundColors
				}],

				// These labels appear in the legend and in the tooltips when hovering different arcs
				labels: _candidates
			},
			options : {
				animation : 
				{
					animateScale : true
				}
			}
		});

		var voteTimesGraph = document.getElementById("vote-time-graph").getContext('2d');

		// Initialize the visits canvas
		var votesBarGraph = new Chart(voteTimesGraph, 
		{
			type : 'line', 
			data : {
				datasets : [{
					label : "# of Votes",
					data : _dayvotes,
					backgroundColor: [
		                '#216ddd3f'
		            ],
		            borderColor: [
		                '#216dddff'
		            ],
		            lineTension: 0
				}],

				labels: _daynumber
			},
			options : {
				scales: {
				 yAxes: [{
				     ticks: {
				         beginAtZero: true,
				         userCallback: function(label, index, labels) {
				             // when the floored value is the same as the value we have a whole number
				             if (Math.floor(label) === label) {
				                 return label;
				             }

				         },
				     }
				 }],
				},
			}
		});
	}

	if ($route.includes("options"))
	{
		$.get("/data/election/" + __id, function(data)
		{
			var options_array = data.options;
			var grid = null;

			var reloadGridFunctionality = (rand) => 
			{
				$('input#election_title, textarea#election_description').characterCounter();
				Materialize.updateTextFields();

				var $allClose = $(".item .close-button");

				$allClose.click((event) => 
				{
					grid.remove(event.target.parentNode.parentNode.parentNode);
					$(event.target).parent().parent().parent().remove();
					grid.layout();
					UpdateLabels();
				});

				// Get all the option_images
				var $image = $(".option_image");
				$image.off('change'); // Remove the event first to disable duplicate event bindings
				$image.change(function(event)
				{
					var $ev = $(event.target).prev();

					// Upload the image to the server and preview the image
					$.ajax({
						url : "/image",
						type : "POST",
						data : new FormData(event.target.parentNode.parentNode),
						contentType: false,       // The content type used when sending data to the server.
						cache: false,             // To unable request pages to be cached
						processData:false,

						success : function(data) 
						{
							$ev.attr("src", data);
							Materialize.toast('Image successfully uploaded', 4000, "alert-information");
						},

						error: function(er)
						{
							Materialize.toast('Could not upload the image', 4000, "alert-error");
							console.log(er);
						}
					});
				});
			}

			var option_template = "";
			var $options_container = $("#options-grid-container");

			var UpdateLabels = (items) =>
			{
				for (var i = 0; i < items.length; i ++)
				{
					var current = items[i];
					$(current._element).find(".note").text("Option #" + (i + 1).toString());
				}
			}

			$.get('/scripts/option-card.hbs', function (data) {

				if (options_array.length > 0)
				{
					for (var i = 0; i < options_array.length; i ++) {
						var current = options_array[i];

						option_template = Handlebars.compile(data);
					    $options_container.append(option_template({
					    	rand : i + 1,
					    	option_title : current.title,
					    	option_description : current.bio,
					    	option_image : (current.image || "/resources/placeholder.png")
					    }));

						grid = new Muuri('.grid', { 
							dragEnabled: true,
							dragSortInterval: 1,
							dragStartPredicate: {
								handle: '.note'
						    }
						}).on('layoutEnd', function (items) {
							UpdateLabels(items);
						});

						UpdateLabels(grid.getItems());
					}
				} else {

					option_template = Handlebars.compile(data);

					grid = new Muuri('.grid', { 
						dragEnabled: true,
						dragSortInterval: 1,
						dragStartPredicate: {
							handle: '.note'
					    }
					}).on('layoutEnd', function (items) {
						UpdateLabels(items);
					}); // Initialize the grid even without items

					UpdateLabels(grid.getItems());
				}

				$(".materialize-textarea").on('input', function() 
				{
					grid.layout();
				});

				reloadGridFunctionality();

				$("#add-options").click(() =>
				{	
					var rand = $options_container.children().length;
					var args = {
						rand : rand + 1,
						option_image : "/resources/placeholder.png"
					};

					grid.add($.parseHTML(option_template(args)));
					reloadGridFunctionality(rand + 1);
					grid.layout();
				});

				$("#save-options").click(() =>
				{	
					// form a json object to be sent
					$json_object = [];
					for (var i = 0; i < $options_container.children().length ;i++)
					{
						var current = $($options_container.children()[i]);

						var title = current.find(".option_title").val();
						var description = current.find(".materialize-textarea").val();
						var image = current.find(".option-img").attr("src");

						$json_object.push({
							title : title,
							bio : description,
							image: image
						});
					}

					$json_output = (JSON.stringify($json_object));

					var formdata = new FormData();  // Create empty formdata
					formdata.append("options", $json_output); // Add the json output to the formdata

					$.ajax({
						url: "/data/election/" + __id,
						data: formdata,
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						processData: false,
						contentType: false,
						type: 'POST',

						success: function(data){
							if (data == "SAVED")
							{
								Materialize.toast('Your election options are saved', 4000, "alert-information");
							}
							else if (data == "ERROR")
							{
								Materialize.toast('Could not save your election options', 4000, "alert-error");
							}
						},

						error : function(er)
						{
							console.error(er);
						}
					});
				});

			}, 'html');
		});
	}

	
}
