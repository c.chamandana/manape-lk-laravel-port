// Get all of the elements in the create form
var $firstname = $("#firstname");
var $lastname = $("#lastname");
var $email = $("#email");
var $nic = $("#nic");
var $contact = $("#contact");
var $password = $("#password");
var $confirm = $("#confirm-password");
var $agreement = document.getElementById('tacs');
var $newsletter = document.getElementById('newsletter');
var $signupButton = $("#signup-button");
var $createForm = $("#create-form");

var errorBottom = "solid 2px red";
var normalBottom = "1px solid #9e9e9e";
var $signuperror = $("#signup-error");


var $passwordValidator = $(".password-validation");
var $passwordLengthError = $("#password-length-error");
var $passwordNumbersError = $("#password-numbers-error");

const checkEmpty = (i) => (i.replace(' ', '') == '');
const validateEmail = (email) => 
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

const hasNumber = (myString) => {
	return /\d/.test(myString);
}
// TODO::Add a has character method for the contact options and NIC options

const validateNIC = (nic) => 
{
	if (nic.length == 10 && nic.toLowerCase().endsWith("v"))
		return true;
	else if (nic.length == 12)
	 	return true;

	return false;
}

const validateContact = (contact) => 
{
	if ((contact.startsWith("+94") && contact.length == 12) || (contact.startsWith("94") && contact.length == 11) || (contact.startsWith("0") && contact.length == 10) || (contact.length == 9))
		return true
	else
		return false;		
}

const validatePassword = () =>
{
	var valid = true;

	// Check if password is valid
	var password = $password.val();
	if (password.length < 6)
	{
		valid = false;
		$passwordValidator.fadeIn();
		$passwordLengthError.slideDown();
	}else {
		$passwordLengthError.slideUp();
	}
	
	if (!hasNumber(password))
	{
		valid = false;
		
		$passwordValidator.fadeIn();
		$passwordNumbersError.slideDown();
	}else{
		$passwordNumbersError.slideUp();
	}

	return valid;
} 

(() => 
{


	$firstname.focusout(() =>
	{
		if (checkEmpty($firstname.val()))	
			$firstname.css("border-bottom", errorBottom);
		else
			$firstname.css("border-bottom", normalBottom);
	});

	$lastname.focusout(() =>
	{
		if (checkEmpty($lastname.val()))	
			$lastname.css("border-bottom", errorBottom);
		else
			$lastname.css("border-bottom", normalBottom);
	});


	$email.focusout(() =>
	{
		if (!validateEmail($email.val()))	
			$email.css("border-bottom", errorBottom);
		else
			$email.css("border-bottom", normalBottom);
	});

	$nic.focusout(() =>
	{
		if (!validateNIC($nic.val()))	
			$nic.css("border-bottom", errorBottom);
		else
			$nic.css("border-bottom", normalBottom);
	});

	$contact.focusout(() =>
	{
		if (!validateContact($contact.val()))	
			$contact.css("border-bottom", errorBottom);
		else
			$contact.css("border-bottom", normalBottom);
	});

	$password.on("input", function()
	{
		validatePassword();
	});

	$password.focusout(() =>
	{
		if (!validatePassword($password.val()))	
			$password.css("border-bottom", errorBottom);
		else
			$password.css("border-bottom", normalBottom);
	});

	$password.focusin(() => 
	{
		$passwordValidator.fadeIn();
	});

	$confirm.focusout(() => 
	{
		if ($password.val() != $confirm.val())	
			$confirm.css("border-bottom", errorBottom);
		else
			$confirm.css("border-bottom", normalBottom);
	})

})();

$(document).ready(function()
{
	$passwordValidator.fadeOut();
});

$signupButton.click(function()
{
	var valid = true;

	if (checkEmpty($firstname.val()))	
	{
		$firstname.css("border-bottom", errorBottom);
		valid = false;
	}
	if (checkEmpty($lastname.val()))	
	{
		$lastname.css("border-bottom", errorBottom);
		valid = false;
	}
	if (!validateEmail($email.val()))	
	{
		$email.css("border-bottom", errorBottom);
		valid = false;
	}
	if (!validateNIC($nic.val()))	
	{
		$nic.css("border-bottom", errorBottom);
		valid = false;
	}
	if (!validateContact($contact.val()))	
	{
		$contact.css("border-bottom", errorBottom);
		valid = false;
	}
	if (!validatePassword($password.val()))	
	{
		$password.css("border-bottom", errorBottom);
		valid = false;
	}
	if ($password.val() != $confirm.val())	
	{
		$confirm.css("border-bottom", errorBottom);
		valid = false;
	}
		
	if (!valid)
	{
		Materialize.toast('Please fill the fields in red correctly', 4000, "alert-error");
	}else{
		if ($agreement.checked)
		{
			// TODO: send the request
			$createForm.submit();

		}else {
			Materialize.toast('You must agree to our terms and conditions', 4000, "alert-error");
		}
	}
	event.preventDefault();
});