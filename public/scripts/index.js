$('.carousel.carousel-slider').carousel({fullWidth: true});

var $leftTestimonial = $(".testimonial.left");
var $rightTestimonial = $(".testimonial.right");

var slide = () =>
{
	$leftTestimonial.carousel("prev");
	$rightTestimonial.carousel("next");

	setTimeout(() => 
	{
		slide();
	}, 5000);
}

slide();

AOS.init({
  duration: 1200,
})

var $arrows = $(".arrows");
var $scrollDarkener = $(".scroll-darkener");

var hideArrows = () =>
{
	$arrows.fadeOut(); $scrollDarkener.fadeOut();
}

var showArrows = () => 
{
	$arrows.fadeIn(); $scrollDarkener.fadeIn();
}

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    
    if (scroll > 10) 
    	hideArrows();
    else 
    	showArrows();
});

if ($(window).scrollTop() > 10)
	hideArrows();


setTimeout(() => 
{
	showArrows();
}, 1000);