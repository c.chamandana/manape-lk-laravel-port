var router = ccrouter();  // Create a new router from ccrouter

router.initialize("/dashboard/tabs/", "", {
    color:  "dodgerblue",
    height: "2",
    initial: "elections"
}, null, refreshPages); 

// Map the change of hashes to the router
router.listen(); // Listen for the incoming hashchanges

var $notifications_button = $("#notifications-button");
var $notifications = $(".notifications");
$(document).ready(function(){
	$notifications_button.click(function() 
	{
		$notifications.toggleClass("show"); 
	});	
});

const openCreateModal = () =>
{
	$("#createModal").modal("open");
}

function refreshPages($route)
{
	$('.dropdown-button').dropdown({
		inDuration: 300,
		outDuration: 225,
		constrainWidth: false, // Does not change width of dropdown to that of the activator
		hover: false, // Activate on hover
		gutter: 0, // Spacing from edge
		belowOrigin: true, // Displays dropdown below the button
		alignment: 'right', // Displays dropdown with edge aligned to the left of button
		stopPropagation: false // Stops event propagation
	});

	$('input#title, textarea#description').characterCounter();

	$('.collapsible').collapsible();

	Materialize.updateTextFields();
	$(".modal").modal();

	$('.datepicker').pickadate({
		selectMonths: true, // Creates a dropdown to control month
		selectYears: 15, // Creates a dropdown of 15 years to control year,
		today: 'Today',
		clear: 'Clear',
		close: 'Ok',
		closeOnSelect: false // Close upon selecting a date,
	});

	$('.timepicker').pickatime({
		default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: false, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: 'Clear', // text for clear-button
		canceltext: 'Cancel', // Text for cancel-button
		autoclose: false, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
	});

	var $createElectionButton = $("#createElectionButton");

	// Create Modal elements
	var $title = $("#title");
	var $description = $("#description");
	var $startdate = $("#startdate");
	var $enddate = $("#enddate");
	var $starttime = $("#starttime");
	var $endtime = $("#endtime");

	var errorBottom = "solid 2px red";
	var normalBottom = "1px solid #9e9e9e";


	const empty = (s) =>
	{
		return (s.replace(" ", "").length == 0)
	}

	$createElectionButton.click(function()
	{
		var title = $title.val();
		var description = $description.val();
		var startdate = $startdate.val();
		var enddate = $enddate.val();
		var starttime = $starttime.val();
		var endtime = $endtime.val();

		var valid = true;

		// Validate values
		if (empty(title))
		{
			valid = false;
			$title.css("border-bottom", errorBottom);
		}else{
			$title.css("border-bottom", normalBottom);
		}

		if (empty(description))
		{
			valid = false;
			$description.css("border-bottom", errorBottom);
		}else{
			$description.css("border-bottom", normalBottom);
		}

		if (empty(startdate))
		{
			valid = false;
			$startdate.css("border-bottom", errorBottom);
		}
		else{
			$startdate.css("border-bottom", normalBottom);
		}

		if (empty(enddate))
		{
			valid = false;
			$enddate.css("border-bottom", errorBottom);
		}else{
			$enddate.css("border-bottom", normalBottom);
		}

		if (empty(starttime))
		{
			valid = false;
			$starttime.css("border-bottom", errorBottom);
		}else{
			$starttime.css("border-bottom", normalBottom);
		}

		if (empty(endtime))
		{
			valid = false;
			$endtime.css("border-bottom", errorBottom);
		}else{
			$endtime.css("border-bottom", normalBottom);
		}

		if (valid)
		{
			// Check time validity
			var format = "D MMMM, YYYY HH:mm";
			var start = moment(startdate + " " + starttime, format);
			var end = moment(enddate + " " + endtime, format);

			// make sure the start date is before end
			valid = start.isBefore(end); 

			if (valid == false)
			{
				$enddate.css("border-bottom", errorBottom);
				$endtime.css("border-bottom", errorBottom);
				Materialize.toast('End date and time must be after start date and time', 4000, "alert-error");

				return false;
			}
		}

		if (valid)
		{
	 		// if valid submit the form
	 		$("#createModalForm").submit(); // Submit the form
	 		$("#createModal").modal("close");
	 	} else {
	 	 	Materialize.toast('Please fill the fields in red correctly', 4000, "alert-error");
		}
	});
}