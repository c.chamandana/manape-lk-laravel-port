var $email = $("#email");
var $password = $("#password");
var $rememberCheckbox = document.getElementById("remember");
var $loginButton = $("#login-button");
var $loginForm = $("#login-form");

var errorBottom = "solid 2px red";
var normalBottom = "1px solid #9e9e9e";

const validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

const validateForm = () =>
{
	var valid = true;

	// Login button is clicked
	if (!validateEmail($email.val()))
	{
		// Email is valid
		$email.css("border-bottom", errorBottom);
		Materialize.toast('Please enter a valid email address to continue', 4000, "alert-error");
		valid = false;
	}else {
		$email.css("border-bottom", normalBottom);
	}

	if ($password.val().replace(" ", "").length == 0)
	{
		// Password is not entered
		$password.css("border-bottom", errorBottom);
		Materialize.toast('Please enter your password to continue', 4000, "alert-error");
		valid = false;
	}else{
		$password.css("border-bottom", normalBottom);
	}

	return valid;
}

$email.focusout(function()
{
	// Login button is clicked
	if (!validateEmail($email.val()))
	{
		// Email is valid
		$email.css("border-bottom", errorBottom);
	}else {
		$email.css("border-bottom", normalBottom);
	}
});

$password.focusout(function()
{
	if ($password.val().replace(" ", "").length == 0)
	{
		// Password is not entered
		$password.css("border-bottom", errorBottom);
	}else{
		$password.css("border-bottom", normalBottom);
	}
})

$loginButton.click(function()
{

	event.preventDefault();

	if (validateForm())
	{
		$loginForm.submit() // submit the form only when the form is valid
	}

});


