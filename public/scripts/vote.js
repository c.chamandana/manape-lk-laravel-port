$(document).ready(function() 
{
	$(".election-description").html(_description.replaceAll("&lt;", "<").replaceAll("&gt;", ">"));

	$(".modal").modal();
	$(".modal-launch").click(function(event)
	{
		var index = this.getAttribute("data-index");
		$("#candidate-name").html(_options[index]["title"]);
		$("#submitter").attr(
			"href", ("/election/vote/" + _id + "/" + (parseInt(index) + 1).toString())
		);
		$("#votemodal").modal("open");
	});
});