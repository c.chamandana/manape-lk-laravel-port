'use strict';

const gulp = require("gulp");
const sass = require("gulp-sass");
const minify = require("gulp-minify-css"); 
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");

// Transpiling the sass files to beauty versions
gulp.task("sass", () => 
{
	return gulp
		.src("sass/main.scss")
		.pipe(sass().on('error', function (err) {
	        this.emit('end');
	      }))
		.pipe(minify())
		.pipe(rename("stylesheet.css"))
		.pipe(gulp.dest("css/"))
});

// Watch command for file changes
gulp.task("watch", () => 
{
	// Watch for Sass file changes and compile them
	gulp.watch("sass/*.scss", ["sass"]);
});