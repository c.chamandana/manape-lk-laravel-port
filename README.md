# GETTING STARTED

1. Open a CLI (Command Prompt, Powershell or Bash) and navigate to the project folder (Use CD command)
2. Install the back-end dependencies using Composer Dependency Manager, using `composer install`. ([Download and Install Composer](https://getcomposer.org/) first if you haven't before)
3. Make a database called `manape.lk` on your localhost MySQL Database. 
4. Migrate the database using PHP Artisan Command `php artisan migrate`, If you cannot just import the SQL file `manape.lk.database.sql` to your `manape.lk` database.
5. Use `php artisan serve` to host the Laravel Application using a Localhost server.
6. Navigate to [http://127.0.0.1:8000](http://127.0.0.1:8000)
