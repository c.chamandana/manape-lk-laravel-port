<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('elections', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('owner');
			$table->string('title');
			$table->string('description', 10000);
			$table->timestamp('startdate')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('enddate')->default('0000-00-00 00:00:00');
			$table->string('status');
			$table->integer('visits')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('elections');
	}

}
