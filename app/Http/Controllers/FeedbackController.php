<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function save(Request $request)
    {
    	$feedback = new Feedback;

    	$name = $request->input('name');
    	$email = $request->input('email');
    	$message = $request->input('message');

    	$feedback->name = $name;
    	$feedback->email = $email;
    	$feedback->message = $message;

    	$feedback->save();

        return redirect()->route('index');
    }
}
