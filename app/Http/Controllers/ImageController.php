<?php

namespace App\Http\Controllers;

use \Input as Input;
use Illuminate\Http\Request;
use Image;
use Storage;

class ImageController extends Controller
{
    public function uploadImage(Request $request)
    {
        if ($_FILES["image"]["tmp_name"] !== null)
        {
            $image      = $request->file('image');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();

            $url = Storage::putFileAs('images', $request->file("image"), $fileName);

            $image->move(public_path("images/"), $fileName);

            echo "/images/" . $fileName;
        }else {
            return false;
        }
    }

    public function downloadImage($id)
    {
        return Image::make($storagePath . "/app/images" . $id)->response();
    }
}
