<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\DataManager;

class SaveController extends Controller
{
    public function election(Request $request, $id)
    {
    	DataManager::setElectionOptions($id, $request->input("options"));
    }
}
