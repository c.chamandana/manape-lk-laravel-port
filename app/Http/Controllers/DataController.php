<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\DataManager;

class DataController extends Controller
{
    public function election($id)
    {
    	$electionData = DataManager::getElectionData($id);
    	return $electionData;
    }
}
