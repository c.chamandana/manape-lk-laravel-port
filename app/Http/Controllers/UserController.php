<?php

namespace App\Http\Controllers;

use App\User;
use App\Libraries\Passman;
use App\Libraries\Cookieman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{

    public function login(Request $request)
    {
        // Get the values from the input
        $email = $request->input("email");
        $password = $request->input("password");
        $remember = $request->input("remember");

        // validate the credentails
        if (User::where("email", $email)->exists())
        {
            // User exists
            $user = User::where("email", $email)->first();

            switch ($user->type) {
                case 'EMAIL':
                        if (Passman::assert($email, $password))
                        {
                            // the password is correct
                            $cookieSaveData = Cookieman::saveCookie($user);
                            $cookie = "";

                            if ($remember === "on")
                            {
                                $cookie = Cookie::make("randInt", $cookieSaveData, (60 * 24) * 30); // Save the cookie for 30 days
                            } else {
                                $cookie = Cookie::make("randInt", $cookieSaveData, 60); // Save the cookie for 30 days
                            }
                            
                            // Return the user to the dashboard
                            return redirect()->route('dashboard')->withCookie($cookie);
                        } else {
                            return Redirect::route("login")->with([
                                "message-class" => "alert-error",
                                "message" => "Sorry, wrong password entered"
                            ]);
                        }
                    break;
                case 'FACEBOOK':
                    return Redirect::route("login");
                    break;
                default:
                    // The password is wrong
                    return Redirect::route("login");
                    break;
            }
           
        } else {
            // User does not exist
            return Redirect::route("login")->with([
                "message-class" => "alert-error",
                "message" => "No user found for \"" . $email . "\"."
            ]);
        }
    }

    public function create(Request $request)
    {
    	// Get all the fields from the input form
    	$firstname = $request->input('firstname');
    	$lastname = $request->input('lastname');
    	$email = $request->input("email");
    	$nic = $request->input("nic");
    	$contact = $request->input("contact");
    	$password = $request->input("password");
    	$confirm = $request->input("confirm");

        $cookie = UserController::createAccount($firstname, $lastname, $email, $nic, $contact, $password, $confirm); // Create the account

        // Return the user to the dashboard
        return redirect()->route('index')->withCookie($cookie);
    }

    public static function createAccount($firstname, $lastname, $email, $nic, $contact, $password, $confirm, $type = "EMAIL", $profile="")
    {

        $user = new User;

        // Validate the data further
        if (User::where("email", $email)->exists())
        {
            // User exists with the same email
            return Redirect::route("signup")->with([
                "message-class" => "alert-error",
                "message" => "The email you entered is already in use"
            ]);
        }
        
        if ($nic != "" && $contact != "")
        {
            if (User::where("nic", $nic)->exists())
            {
                // User exists with the same NIC number
                return Redirect::route("signup")->with([
                    "message-class" => "alert-error",
                    "message" => "The NIC number you entered is already used by another user"
                ]);
            }
            
            if (User::where("contact", $contact)->exists())
            {
                // User exists with the same mobile number
                return Redirect::route("signup")->with([
                    "message-class" => "alert-error",
                    "message" => "The phone number you entered is already in use"
                ]);
            } 
        }
        
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->email = $email;
        $user->nic = $nic;
        $user->contact = $contact;
        $user->profile = $profile;

        if ($password !== "")
        {
            // Hash the password
            $hash = Passman::hash($password);
            $user->password = ($hash); // Save the hashed password
        } else {
            $user->password = ""; // Assign an empty password
        }

        $user->type = $type; // Set the account type

        $user->save();

        // Save the user in the tempcookies
        $cookieSaveData = Cookieman::saveCookie($user);
        return $cookie = Cookie::make("randInt", $cookieSaveData, (60 * 24) * 30); // Save the cookie for 30 days
    
    }

    function signOut(Request $request)
    {
        $user = Cookieman::getUser(); // Get the user inforamtion first

        if ($user !== false)
        {
            $cookie = Cookie::forget("randInt"); // Forget the cookie from the browser
            return Redirect::route("login")->with([
                "message-class" => "alert-information",
                "message" => "You've successfully logged out"
            ])->withCookie($cookie);;
        }
    }
}
