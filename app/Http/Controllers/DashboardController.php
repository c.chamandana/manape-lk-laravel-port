<?php

namespace App\Http\Controllers;


use App\Http\Middleware\UserRetriever;
use App\Vote;
use Illuminate\Http\Request;
use App\Libraries\Passman;
use App\Libraries\Cookieman;	
use App\Election;
use App\Libraries\DataManager;
use App\Libraries\VoteManager;
use App\User;
use App\Organization;
use Session;
use Storage;
use Illuminate\Support\Facades\Redirect;


class DashboardController extends Controller
{
    public function open(Request $request)
	{
    	return view("dashboard.index"); // load the dashboard once the user is requested
    }

    public static function loadPage()
    {
       
    }

    public function election($id)
    {
        $user = Cookieman::getUser(); // Get the user information from the cookies

        if ($user !== null)
        {
            $election = Election::where("id", $id)->first();
            $electionData = DataManager::getElectionData($id);
            $catVotes = VoteManager::categorizeVotes($id);

            // Confirm the owner of the election
            if ($election->owner == $user->email)
            {
                Session::flash("election", $election);
                Session::flash("election-data", $electionData);
                Session::flash("categorized-votes", $catVotes);

                return view("dashboard.election"); // Show the election dashboard
            } else {
                return Redirect::route("login")->with([
                    "message-class" => "alert-error",
                    "message" => "You are not authenticated to manage that election"
                ]);
            }
        }            
    }

    public function tab($id, $tab)
    {
        $user = Cookieman::getUser(); // Get the user information from the cookies

        if ($user !== null)
        {
            $election = Election::where("id", $id)->first();
            

            $steps = [

            ];

            // If organization settings are saved or not
            if (Organization::where("owner", $user->email)->exists())
            {
                $steps["organization"] = true;
            }else{
                $steps["organization"] = false;
            }

            // If election options are added or not
            if (Storage::disk("elections")->exists($id . ".json"))
            {
                $data = json_decode(Storage::disk("elections")->get($id . ".json"), true); // Decode the content in the json files
                $steps["options"] = (sizeof($data["options"]) != 0);
            }else {
                $steps["options"] = false;
            }

            $electionData = DataManager::getElectionData($id);
            $catVotes = VoteManager::categorizeVotes($id);

            // Confirm the owner of the election
            if ($election->owner == $user->email)
            {
                Session::flash("election", $election);
                Session::flash("election-data", $electionData);
                Session::flash("launch-steps", $steps);
                Session::flash("categorized-votes", $catVotes);

                return view("dashboard.election." . $tab); // Show the election dashboard
            } else {
                return Redirect::route("login")->with([
                    "message-class" => "alert-error",
                    "message" => "You are not authenticated to manage that election"
                ]);
            }
        }   
    }
}
