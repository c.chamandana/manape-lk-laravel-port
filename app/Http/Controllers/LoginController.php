<?php

namespace App\Http\Controllers;

use Socialite;
use App\User;
use App\Libraries\Cookieman;
use Google_Client;
use Illuminate\Support\Facades\Cookie;
use Google_Service_People;
use Illuminate\Support\Facades\Redirect;


class LoginController extends Controller
{
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->setScopes(["public_profile", "email"])->redirect();
    }

    /**
    * Redirect the user to the Google Plus authentication page.
    *
    * @return \Illuminate\Http\Response
    */
    public function redirectToGooglePlus()
    {
        return Socialite::driver('google')
            ->scopes(['profile', 'email'])
            ->redirect();   
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();

        // Create the user account
        $name = $user->name;
        $email = $user->email;

        $cookie = "";

        if (User::where("email", $email)->exists())
        {
            $savedUser = User::where("email", $email)->first();

            if ($savedUser->type == "FACEBOOK")
            {
                // Give access to $email for the user
                $cookieSaveData = Cookieman::saveCookie($user);
                $cookie = Cookie::make("randInt", $cookieSaveData, (60 * 24) * 30);
            }else{
                return Redirect::route("login")->with([
                    "message-class" => "alert-error",
                    "message" => "Your account is not linked to a Facebook Account"
                ]);
            }
        } else {
            // Create the user
            $cookie = UserController::createAccount($name, "", $email, "", "", "", "", "FACEBOOK");
        }

        if($cookie)
        {
            // Return the user to the dashboard
            return redirect()->route('dashboard')->withCookie($cookie);
        }
    }

    public function handleGooglePlusCallback()
    {
        $user = Socialite::driver("google")->user();

        $email = $user->email;
        $firstname = $user->user["name"]["givenName"];
        $lastname = $user->user["name"]["familyName"];
        $profile = $user->user["image"]["url"];

        $cookie = "";

        if (User::where("email", $email)->exists())
        {
            $savedUser = User::where("email", $email)->first();

            if ($savedUser->type == "GOOGLE")
            {
                // Give access to $email for the user
                $cookieSaveData = Cookieman::saveCookie($user);
                $cookie = Cookie::make("randInt", $cookieSaveData, (60 * 24) * 30);
            }else {
                return Redirect::route("login")->with([
                    "message-class" => "alert-error",
                    "message" => "Your account is not linked to a Google Account"
                ]);
            }
        } else {
            // Create the user
            $cookie = UserController::createAccount($firstname, $lastname, $email, "", "", "", "", "GOOGLE");
        }

        if ($cookie)
        {
            // Return the user to the dashboard
            return redirect()->route('dashboard')->withCookie($cookie);
        }
    }
}