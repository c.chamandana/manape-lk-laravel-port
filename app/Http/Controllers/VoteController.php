<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Cookieman;
use App\User;
use App\Election;
use App\Vote;
use Illuminate\Support\Facades\Redirect;

class VoteController extends Controller
{
    public function vote(Request $request, $id)
    {
    	return view("vote.vote"); // Return the voting view
    }

    public function submitVote(Request $request, $id, $vote)
    {
    	$user = Cookieman::getUser(); // Get the user information
    	$election = Election::where("id", $id)->first(); // Get the election

    	if ($user->email == $election->owner)
    	{
    		// Owner is trying to vote on the election
    		return Redirect::route("dashboard")->with([
                "message-class" => "alert-error",
                "message" => "You cannot vote on your own election, use &quot;Vote Preview&quot; instead"
            ]);
    	} else {

    		// Check if the user has already voted on the election
    		$votes = Vote::where("electionid", $id); // Get all of the votes casted on the election

    		if ($votes->where("castedby", $user->email)->exists())
    		{
    			echo "USER HAS CASTED";
    			// User has already voted on the election
    			return Redirect::route("dashboard")->with([
	                "message-class" => "alert-error",
	                "message" => "You have already voted on that election"
	            ]);
    		} else {
    			// Cast the vote
    			if ((is_numeric($id)) || (is_numeric($vote)))
    			{
	    			$v = new Vote;
	    			$v->castedby = $user->email;
	    			$v->electionid = intval($id);
	    			$v->vote = intval($vote);

	    			$v->save(); 

					return Redirect::route("thankyou"); // Redirect the user to the thank you page

	    		} else {
	    			return Redirect::route("dashboard")->with([
		                "message-class" => "alert-error",
		                "message" => "Invalid voting sequence found"
		            ]);	
	    		}
    		}
    	}
    }
}
