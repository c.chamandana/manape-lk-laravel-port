<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\User;
use App\Election;
use App\Libraries\Passman;
use App\Libraries\Cookieman;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

// Set the default timezone to Asia Colombo
date_default_timezone_set('Asia/Colombo');

class ElectionController extends Controller
{
	public static function notEmpty($s)
	{
		$valid = true;

		foreach ($s as $st) 
		{
			$valid = (strlen(str_replace(" ", "", $st)) != 0);

			if ($valid == false)
				return $valid;
		}

		return $valid;
	}

    public function create(Request $request)
    {
		$title = $request->input("title");
		$description = $request->input("description");
		$startdate = $request->input("startdate");
		$enddate = $request->input("enddate");
		$starttime = $request->input("starttime");
		$endtime = $request->input("endtime");

		// Process the data
		if (ElectionController::notEmpty([$title, $description, $startdate, $enddate, $starttime, $endtime]))
		{
			$datetimeFormat = "j F, Y G:i";

			// Check for date time validity
			$start = DateTime::createFromFormat($datetimeFormat, $startdate . " " . $starttime);
			$end = DateTime::createFromFormat($datetimeFormat, $enddate. " ". $endtime);

			if ($start >= $end)
			{
				// Invalid time
				return Redirect::route("dashboard")->with([
	                "message-class" => "alert-error",
	                "message" => "Start date cannot be greater than or equal to end date"
	            ]);

			} else {
				// Create the election in the database
				$user = Cookieman::getUser();

				if ($user != false)
				{
					$election = new Election;
					$election->owner = $user->email;
					$election->title = $title;
					$election->description = $description;
					$election->status = "DRAFT";
					$election->startdate = $start;
					$election->enddate = $end;

					$election->save();

					return Redirect::route("dashboard");
				}else{
					return Redirect::route("login")->with([
		                "message-class" => "alert-error",
		                "message" => "You need to login in order to create an election"
		            ]);
				}
			}			

		}else{
			return Redirect::route("dashboard")->with([
                "message-class" => "alert-error",
                "message" => "Please enter valid information to create an election"
            ]);
		}
    }
}
