<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Passman;
use App\Election;
use App\Libraries\Cookieman;	
use App\Organization;
use App\User;
use DateTime;
use Illuminate\Support\Facades\Redirect;

class SettingsController extends Controller
{

    public function deleteElection(Request $request, $id)
    {
        $election = Election::where("id", $id)->first();
        // Confirm the ownership
        if (SettingsController::confirmElectionOwnership($election))
        {
            // Delete the record from the database
            $election->delete(); 

            return Redirect::route("dashboard")->with([
                "message-class" => "alert-information",
                "message" => "&quot;" . $election->title . "&quot; has been deleted"
            ]);
        } else {
            return Redirect::route("login")->with([
                "message-class" => "alert-error",
                "message" => "You do not own that election"
            ]);
        }
    }

    public static function notEmpty($s)
    {
        $valid = true;

        foreach ($s as $st) 
        {
            $valid = (strlen(str_replace(" ", "", $st)) != 0);

            if ($valid == false)
                return $valid;
        }

        return $valid;
    }

    public static function confirmElectionOwnership($election)
    {
        $user = Cookieman::getUser();
        return ($election->owner == $user->email);
    }

    public function electionTiming(Request $request, $id)
    {
        $election = Election::where("id", $id)->first();
        // Confirm ownership
        if (SettingsController::confirmElectionOwnership($election))
        {
            // Ownership confirmed
            // Validate the input
            $startdate = $request->input("startdate");
            $enddate = $request->input("enddate");
            $starttime = $request->input("starttime");
            $endtime = $request->input("endtime");

            if (SettingsController::notEmpty([$startdate, $enddate, $starttime, $endtime]))
            {
                $datetimeFormat = "j F, Y G:i";

                // Check for date time validity
                $start = DateTime::createFromFormat($datetimeFormat, $startdate . " " . $starttime);
                $end = DateTime::createFromFormat($datetimeFormat, $enddate. " ". $endtime);

                if ($start >= $end)
                {
                    // Invalid time
                    return "TIME ERROR";

                } else {

                    // Date and time is validated
                    $election->startdate = $start;
                    $election->enddate = $end;

                    $election->save();

                    return "SAVED";
                }
            } else {
                return "INPUT ERROR";
            }
        } else {
            // Ownership not confirmed
            echo "ERROR";
        }
    }

    public function electionDetails(Request $request, $id)
    {
        $election = Election::where("id", $id)->first();
        // Confirm ownership
        if (SettingsController::confirmElectionOwnership($election))
        {
            // Ownership confirmed
            $election->title = $request->input("title");
            $election->description = $request->input("description");
            $election->save();
            
            echo "SAVED";
        } else {
            // Ownership not confirmed
            echo "ERROR";
        }
    }

    public function saveOrganization(Request $request)
    {
    	$user = Cookieman::getUser();

    	// Get data from the input form
      	$org_name = $request->input("org_name");
    	$org_location = $request->input("org_address");
    	$org_handle = $request->input("org_handle");

    	if (Organization::where("owner", $user->email)->exists())
		{
			// Organization need to be edited not created		
			$org = Organization::where("owner", $user->email)->first();

			// Update the settings
			$org->owner = $user->email;
			$org->name = $org_name;
			$org->handle = $org_handle;
			$org->location = $org_location;

			$org->save(); // Create the organization

			return Redirect::route("dashboard")->with([
                "message-class" => "alert-information",
                "message" => "Your organization settings saved!"
            ]);

		} else {

			// Organization should be created
			$org = new Organization;
			$org->owner = $user->email;
			$org->name = $org_name;
			$org->handle = $org_handle;
			$org->location = $org_location;

			$org->save(); // Create the organization

			return Redirect::route("dashboard")->with([
                "message-class" => "alert-information",
                "message" => "Your organization settings saved!"
            ]);
		}
    }

    public function saveUser(Request $request)
    {
    	$user = Cookieman::getUser();

    	// Get the data from the input form
    	$firstname = $request->input("firstname");
    	$lastname = $request->input("lastname");
    	$email = $request->input("email");
    	$nic = $request->input("nic");
    	$contact = $request->input("contact");

    	// Save these settings
    	$user->firstname = $firstname;
    	$user->lastname = $lastname;
    	$user->nic = $nic;
    	$user->contact = $contact;

    	$user->save();

    	return Redirect::route("dashboard")->with([
            "message-class" => "alert-information",
            "message" => "Your profile settings saved!"
        ]);
    }

    public function launchElection(Request $request, $id)
    {
        $user = Cookieman::getUser(); // Get the user information first

        // Verify election ownership
        $election = Election::where("id", $id)->first();

        if ($election->owner == $user->email)
        {
            // Ownership verified
            $election->status = "LIVE";
            $election->save(); // Set the status for the election

            return Redirect::route("dashboard")->with([
                "message-class" => "alert-information",
                "message" => "Congratulations your election is live"
            ]);
        } 
        else 
        {
            // Ownership is not verified
            return Redirect::route("login")->with([
                "message-class" => "alert-error",
                "message" => "You do not own that election"
            ]);
        }
    }

    public function abortElection(Request $request, $id)
    {
        $user = Cookieman::getUser(); // Get the user information first

        // Verify election ownership
        $election = Election::where("id", $id)->first();

        if ($election->owner == $user->email)
        {
            // Ownership verified
            $election->status = "DRAFT";
            $election->save(); // Set the status for the election

            return Redirect::route("dashboard")->with([
                "message-class" => "alert-information",
                "message" => "Your election is aborted"
            ]);
        } 
        else 
        {
            // Ownership is not verified
            return Redirect::route("login")->with([
                "message-class" => "alert-error",
                "message" => "You do not own that election"
            ]);
        }
    }

    public function saveAppearance(Request $request)
    {

    }
}
