<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\Cookieman;
use App\Libraries\ElectionSorter;
use App\Libraries\VoteManager;
use App\Election;
use App\User;
use App\Organization;
use Session;

class DashboardReciever
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Set the default timezone
        $user = Cookieman::getUser(); // Get the user information from the cookies

        $elections = "";
        if ($user != null)
            $elections = Election::where("owner", $user->email)->get();

        // Get all the votes casted by user
        $votes = VoteManager::getVotes($user);
        // Get sorted elections
        $sortedElections = ElectionSorter::sortElections($elections);

        if ($user != false)
        {
            Session::flash("elections", $sortedElections);
            Session::flash("votes", $votes);

            if (Organization::where("owner", $user->email)->exists())
            {
                $org  = Organization::where("owner", $user->email)->first();
                Session::flash("organization", $org); // Flash the organization settings
            }
        }
        
        return $next($request);
    }
}
