<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\Cookieman;
use Illuminate\Support\Facades\Redirect;
use Session;

class Authenticator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Cookieman::getUser(); // Get the user information from the cookies

        if ($user == false)
        {
            // User is logged in
            return Redirect::route("login")->with([
                "message-class" => "alert-error",
                "message" => "Please login to continue"
            ]);
        }
        
        return $next($request);
    }
}
