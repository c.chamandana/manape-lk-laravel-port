<?php

namespace App\Http\Middleware;

use Closure;
use App\Election;
use App\User;

class ElectionVisitsCounter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $electionid = $request->route('id'); // Get the id of the requested election
        $election = Election::where("id", $electionid)->first(); // Get the election requested

        if ($election->visits == null)
        {
            $election->visits = 1; // Initialize the election visits
        }
        else
        {
            $election->visits = ($election->visits + 1); // Increment the election visits
        }

        $election->save(); // Save the election visits count

        return $next($request);
    }
}
