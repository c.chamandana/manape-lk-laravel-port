<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\Cookieman;
use App\Libraries\ElectionSorter;
use App\Libraries\VoteManager;
use App\Election;
use App\User;
use Session;

class UserRetriever
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Set the default timezone
        $user = Cookieman::getUser(); // Get the user information from the cookies

        if ($user != false)
        {
            // User is logged in
            Session::flash("email", $user->email);
            Session::flash("user", $user);
            Session::flash("login", "true");
        }
        
        return $next($request);
    }
}
