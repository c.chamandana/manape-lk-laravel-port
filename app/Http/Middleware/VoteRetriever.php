<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Organization;
use App\User;
use App\Election;
use App\Libraries\Cookieman;

class VoteRetriever
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $electionid = $request->route('id');
        $election = Election::where("id", $electionid)->first();

        if (Organization::where("owner", $election->owner)->exists())
        {
            $org = Organization::where("owner", $election->owner)->first(); // Get the organization information;
            Session::flash("v-org", $org); // Flash the org 
        }

        return $next($request);
    }
}
