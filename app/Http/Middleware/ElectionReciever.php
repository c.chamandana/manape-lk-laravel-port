<?php

namespace App\Http\Middleware;

use Closure;
use App\Election;
use App\Libraries\Cookieman;
use App\User;
use App\Libraries\DataManager;
use Session;
use App\Vote;
use Illuminate\Support\Facades\Redirect;

class ElectionReciever
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $electionid = $request->route('id');
        $election = Election::where("id", $electionid)->first(); // Find the actual election
        $electiondata = DataManager::getElectionData($electionid); // Get election data form the data manager
        $user = Cookieman::getUser(); // Get the user information from the cookies

        $uservotes = Vote::where("castedby", $user->email); // Votes casted by the user
        if ($uservotes->where("electionid", $electionid)->exists())
        {
            Session::flash("user-vote", $uservotes->where("electionid", $electionid)->first()); // Flash the users vote to the engine
        }

        // Check if the user owns the election
        if ($election->owner == $user->email)
        {
            // Return the user to the dashboard
            return Redirect::route("dashboard")->with([
                "message-class" => "alert-error",
                "message" => "You cannot vote on your own election, use &quot;Vote Preview&quot; instead"
            ]);
        } else {
            // Send the data to the view
            Session::flash("election" , $election);
            Session::flash("election-data", $electiondata);
        }

        return $next($request);
    }
}
