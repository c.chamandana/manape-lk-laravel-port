<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
use App\Election;

class TimeValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        date_default_timezone_set('Asia/Colombo'); // Set the default timezone to Asia Colombo
        $electionid = $request->route('id'); // Get the id of the requested election
        $election = Election::where("id", $electionid); // Find the actual election

        // Check if the date and time are valid
        $startvalid = $election->where("startdate", "<=", date("Y-m-d H:i:s"))->exists();
        $endvalid = $election->where("enddate", ">=", date("Y-m-d H:i:s"))->exists();

        if ($startvalid && $endvalid)
        {
            // Valid timing

        } else {
            return Redirect::route("dashboard")->with([
                "message-class" => "alert-error",
                "message" => "The election is not yet started or has ended"
            ]);
        }

        return $next($request);
    }
}
