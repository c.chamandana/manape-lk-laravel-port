<?php

namespace App\Libraries;

use App\User;
use App\CookieTemp;
use App\Libraries\Passman;
use Illuminate\Support\Facades\Cookie;

class Cookieman
{
	public static function generateRandom($length)
	{
		$r = "";

		for ($i=0; $i < $length; $i++) { 
			$c = rand(0,9);
			$r = $r . strval($c);
		}

		return $r;
	}

	public static function saveCookie($user)
	{
		$email = $user->email; // Hashed email
		$password = "";
		if (isset($user->password))
			$password = $user->password; // Hashed password 

		$random = Cookieman::generateRandom(250); // Generate 250 lengthed random number key

		while (CookieTemp::where("random", $random)->exists())
		{
			$random = Cookiman::generateRandom(250); // Generate 250 lengthed random number key
		}

		$cookietemp = new CookieTemp;
		$cookietemp->email = $email;
		
		if ($password != "")
			$cookietemp->password = $password;

		$cookietemp->random = $random;

		$cookietemp->save(); // Save the cookietemp

		return $random;
	}

	public static function getUser()
	{
		$cookie = Cookie::get("randInt"); // Get the random integer
		if ($cookie !== null)
		{
			$cookieTemp = CookieTemp::where("random", $cookie)->first();
			
			if ($cookieTemp !== null){

				$email = $cookieTemp->email;

				$hashedPassword = $cookieTemp->password;

				$user = User::where("email", $email)->first();

				if (Passman::assert($email, $hashedPassword))
				{
					// the password is valid
					return $user;
				}
				elseif ($hashedPassword == $user->password)
				{
					return $user;
				}
				else
				{
					// the password is invalid
					return false;
				}
			}else{
				// The cookie is invalid
			}
		}else {
			// didn't have a cookie saved
			return false;
		}
	}
}