<?php 

namespace App\Libraries;

use App\Vote;
use App\Election;
use App\Libraries\DataManager;


class VoteManager
{
	public static function getVotes($user)
	{
		$votes = Vote::where("castedby", $user->email)->get(); // Get all the votes casted by the user
		
		$return = []; 

		if (sizeof($votes) == 0)
		{
			return [];
		} else {
			foreach ($votes as $vote)
			{
				// Get the election
				$election = Election::where("id", $vote->electionid)->first();
				// Get election data
				$data = DataManager::getElectionData($vote->electionid);	

				$return[] = [
					"vote" => $vote,
					"data" => $data,
					"election" => $election
				];
			}
		}

		return $return;
	}

	public static function categorizeVotes($electionid)
	{
		$votes = Vote::where("electionid", $electionid)->get();
		$data = DataManager::getElectionData($electionid);	

		$categories = [];

		$votesFor = [];

		for ($i=0; $i < sizeof($data["options"]); $i++) { 
			$current = $data["options"][$i];
			$count = $votes->where("vote", ($i + 1))->count();
			$votesFor[$current["title"]] = $count;
		}

		// Add per month categorization for the votes
		date_default_timezone_set('Asia/Colombo'); // Set the default timezone to Asia Colombo
		$today = date("Y-m");

		$month = [];
		$sortedMonth = [];
		$days = date("t"); // Get number of days in the month

		foreach ($votes as $vote)
		{
			$create_date = $vote->created_at;

			if ((strpos($create_date, $today) !== false))
			{
				if (isset($month[$create_date->day]) == false)
					$month[strval($create_date->day)] = 1;
				else 
					$month[strval($create_date->day)] = $month[strval($create_date->day)] + 1;
			}
		}

		// Fill in the other days
		for ($i=1; $i <= $days; $i++) { 
			if (isset($month[$i]) == false)
				$sortedMonth[$i] = 0;
			else 
				$sortedMonth[$i] = $month[$i];
		}

		// Zoom into the graph
		$today_date = date("j");
		$zoomed_month = [];

		if ($today_date < 10)
		{
			for ($i=1; $i <= 10; $i++) { 
				$zoomed_month[$i] = $sortedMonth[$i];
			}
		} else {
			for ($i = $today_date; $i < 10; $i--) { 
				$zoomed_month[$i] = $sortedMonth[$i];
			}
		}

		$categories = [
			"votesFor" => $votesFor,
			"total" => $votes->count(),
			"month" => $zoomed_month
		];

		return $categories;
	}
}