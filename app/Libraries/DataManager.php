<?php


namespace App\Libraries;

use App\User;
use Storage;

class DataManager
{
	public static function getElectionData($id)
	{
		if (Storage::disk("elections")->exists($id . ".json"))
		{
			$json = Storage::disk("elections")->get($id . ".json"); // Get the data through the Storage component
			$data = json_decode($json, true);
	 		return $data;
	 	} else {
	 		return [ "options" => [] ];
	 	}
	}

	public static function setElectionOptions($id, $jsondata)
	{
		try {
			if (Storage::disk("elections")->exists($id . ".json"))
			{
				$json = Storage::disk("elections")->get($id . ".json"); // Get the data through the Storage component
				$data = json_decode($json, true);
				$data["options"] = json_decode($jsondata, true);
				$savedata = json_encode($data);
				Storage::disk("elections")->put($id . ".json", $savedata); // Save the file
			}
			else 
			{
				$savedata = ["options" => json_decode($jsondata, true)];
				Storage::disk("elections")->put($id . ".json", json_encode($savedata));
			}

			echo "SAVED";
		} catch (Exception $e) {
			echo "ERROR";
		}
		
	}
}