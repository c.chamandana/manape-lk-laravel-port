<?php

namespace App\Libraries;

use App\User;

class Passman
{
	public static function hash($password)
	{
		return password_hash($password, PASSWORD_DEFAULT);
	}

	public static function assert($email, $password)
	{
		$user = User::where("email", $email)->first();
		return password_verify($password, $user->password);
	}
}