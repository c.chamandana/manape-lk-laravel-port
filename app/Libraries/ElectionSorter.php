<?php

namespace App\Libraries;

use DateTime;

class ElectionSorter
{
	public static function sortElections($elections)
    {
        // Set the default timezone to Sri Lanka
        date_default_timezone_set('Asia/Colombo');

        $format = "Y-m-d H:i:s";
        $today = date($format);
 		$current = DateTime::createFromFormat("Y-m-d H:i:s", $today);
 
        // Get the available elections
        $live = [];
        $draft = [];
        $active = [];
        $ended = [];

        foreach ($elections as $election)
        {
        	$end = DateTime::createFromFormat($format, $election->enddate);
        	$start = DateTime::createFromFormat($format, $election->startdate);

        	if ($end > $current)
        	{
        		if ($election->status == "DRAFT")
        		{
        			$draft[] = $election;
        		}else{
	        		if ($start < $current)
	        		{
		        		$live[] = $election;
		        	} else {
		        		$active[] = $election;
		        	}
		        }
        	} else {
        		$ended[] = $election;
        	}
        }

        return [
        	"draft" => $draft,
        	"live" => $live,
        	"active" => $active,
        	"ended" => $ended
        ];
    }
}