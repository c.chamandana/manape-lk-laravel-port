<a id="contact"></a>
<div class="contact-form map-background" style="background-image:url('resources/uielems/map.png');">
	<div class="container">
		<div class="row">
			<div data-aos="fade-right" class="col l6 m6 s12 mobile-col-space">
				<span class="gray-topic">Say hello to us!</span>
				<span class="blue-subtext" style="letter-spacing:1px;">We would love to hear from you!</span>

				<div class="large-space"></div>

				<span class="blue-subtext">Address:</span>
				<span class="gray-subtext">148, Golummahara, Delgoda, Sri Lanka</span>

				<div class="space"></div>

				<span class="blue-subtext">Phone:</span>
				<span class="gray-subtext">+94 77 96 88 569</span>

				<div class="space"></div>

				<span class="blue-subtext">Email:</span>
				<span class="gray-subtext">hello@manape.lk</span>

			</div>
			<div  data-aos="fade-left" class="col l6 m6 s12">
				<form method="POST" autocomplete="off"  action="feedback" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="input-field"><input id="name" type="text" placeholder="Name" name="name"></div>
					<div class="input-field"><input id="email" type="email" placeholder="E-mail" name="email"></div>

					<div class="input-field col s12">
						<textarea name="message" data-length="1000" id="message" class="materialize-textarea"></textarea>
						<label for="message">Message</label>
			        </div>

		        	<div style="text-align:center">
			        	<button class="blue-button big waves-effect waves-dark" style="margin-top:25px;">Submit</button>
			        </div>
				</form>
			</div>
		</div>
	</div>
</div>