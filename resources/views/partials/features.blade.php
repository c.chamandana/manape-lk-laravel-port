
<div class="feature container" style="margin-top:100px;margin-bottom:100px;">
	<div class="row">
		<div class="col l6 m6 s12 feature-content mobile-col-space">
			<p class="main-feature-text">
				Your elections.<br>
				Any device. Any location.
			</p>

			<p class="feature-subtitle-text">You're always in control with Manape.lk. It's easy to build and customize an election</p>

			<a href="create"><button class="waves-effect waves-dark feature-checkout-button">Get started</button></a>
		</div>

		<div class="feature-image col l6 m6 s12">
			<img src="resources/uielems/a6b76f51677371.58f669e022041.png">
		</div>
	</div>
</div>

<a id="services"></a>
<div class="feature container">
	<div class="row feature-content">
		<div class="col l5 m5 s12 feature-image" style="padding-right:64px">
			<img data-aos="fade-right" src="resources/uielems/2.gif">
		</div>
		<div class="col l7 m7 s12">
			<div class="feature-list">

				<table>
					<tr class="feature-item"  data-aos="fade-left">
						<td>
							<img src="resources/uielems/33.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Secure voting</span>
							<span class="feature-description">Each voter has a unique "Voter ID" and "Voter Key" and can only vote once</span>
						</td>
					</tr>
					<tr class="feature-item"  data-aos="fade-left">
						<td>
							<img src="resources/uielems/37.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Custom design</span>
							<span class="feature-description">Personalize your election with your organization's logo and colors. No HTML/CSS knowledge is necessary</span>
						</td>
					</tr>
					<tr class="feature-item" data-aos="fade-left">
						<td>
							<img src="resources/uielems/34.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Results Tabulation</span>
							<span class="feature-description">Election results are automatically calculated and presented with beautiful charts</span>
						</td>
					</tr>
					<tr class="feature-item" data-aos="fade-left">
						<td>
							<img src="resources/uielems/35.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Mobile Ready</span>
							<span class="feature-description">Elections are optimized for desktop and mobile devices. Voters can vote form a web browser or our iOS  &amp; Android Apps</span>
						</td>
					</tr>
					<tr class="feature-item" data-aos="fade-left">
						<td>
							<img src="resources/uielems/32.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Shareable</span>
							<span class="feature-description">Your poll is automatically optimized for Facebook and Twitter. This ensures maximum social engagement. Spread the love for your poll</span>
						</td>
					</tr>
					<tr class="feature-item" data-aos="fade-left">
						<td>
							<img src="resources/uielems/31.jpg" class="feature-item-image">
						</td>
						<td>
							<span class="feature-title">Amazing support</span>
							<span class="feature-description">Have a question? Need Help? We're one call/email away. Our average response time is 10 minutes!</span>
						</td>
					</tr>
				</table>

			</div>
		</div>	
	</div>
</div>

<div class="feature blue-background" style="padding-top:50px;margin-bottom:0 !important">
	<div class="container" style="overflow:hidden">

		<div class="row" style="margin-bottom:0 !important">
			<div class="col l6 m6 s12">
				<span class="feature-title" data-aos="fade-right">Building an election is easy</span>
				<span class="feature-subtitle" data-aos="fade-right">You're always in control with Manape.lk. It's easy to build and customize an election</span>

				<div class="steps">

					<table>
						<tr>
							<td data-aos="fade-right"><i class="fa fa-plus-circle fa-2x"></i></td>
							<td data-aos="fade-left">
								<div class="step-title">Create the election</div>
								<div class="step-description">Add questions (i.e positions) to your new election and add options (candidates, measures, write-in fields, etc.) to your questions. Add a photo and or short bio.</div>
							</td>
						</tr>
						<tr>
							<td data-aos="fade-right"><i class="fa fa-rocket fa-2x"></i></td>
							<td data-aos="fade-left">
								<div class="step-title">Launch the election</div>
								<div class="step-description">When you're done customizing the election, you can schedule a start/end date or immediately launch it</div>
							</td>
						</tr>
						<tr>
							<td data-aos="fade-right"><i class="fa fa-area-chart fa-2x"></i></td>
							<td data-aos="fade-left">
								<div class="step-title">Monitor Results</div>
								<div class="step-description">Watch the results of your election in real-time</div>
							</td>
						</tr>
						<tr>
							<td data-aos="fade-right"><i class="fa fa-share-square-o fa-2x"></i></td>
							<td data-aos="fade-left">
								<div class="step-title">Share the election</div>
								<div class="step-description">At the end of the election you have the option to publish and share the results with your vote</div>
							</td>
						</tr>
					</table>

				</div>

			</div>

			<div class="col l6 m6 s12">
				<img data-aos="fade-left" class="hide-on-small-only" src="resources/uielems/step_image.png" style="height:400px;">
			</div>

		</div>
		<a id="reviews"></a>
		<p data-aos="fade" style="font-size:18px;opacity:0.7;font-weight:bold;text-align:center;margin-top:100px;margin-bottom:75px;">What our customers are saying</p>

		<div class="row" data-aos="fade">

			<?php 
				$filedata1 = file_get_contents("data/testimonials-part1.json");
				$filedata2 = file_get_contents("data/testimonials-part2.json");

				$jsonObject1 = json_decode($filedata1);
				$jsonObject2 = json_decode($filedata2);
			 ?>

			<div class="col l6 m6 s12">
				<div class="testimonial left carousel carousel-slider center" data-indicators="true">
				<div class="carousel-fixed-item center">
				</div>

					<?php foreach ($jsonObject1 as $object) { ?>

					<div class="carousel-item white-text" href="#one!">
						<figure class="snip1192">
						  <blockquote><?php echo $object->testimonial ?></blockquote>
						  <div class="author">
						    <img src="<?php echo $object->profile ?>" alt="sq-sample1"/>
						    <h5><?php echo $object->user ?> <span> <?php echo $object->company ?></span></h5>
						  </div>
						</figure>
					</div>

					<?php } ?>
					
				</div>
			</div>
			<div class="col l6 m6 s12">
				<div class="testimonial right carousel carousel-slider center" data-indicators="true">
					<div class="carousel-fixed-item center">
						<div class="background-dropper"></div>
					</div>
				
					<?php foreach ($jsonObject2 as $object) { ?>

					<div class="carousel-item white-text" href="#one!">
						<figure class="snip1192">
						  <blockquote><?php echo $object->testimonial ?></blockquote>
						  <div class="author">
						    <img src="<?php echo $object->profile ?>" alt="sq-sample1"/>
						    <h5><?php echo $object->user ?> <span> <?php echo $object->company ?></span></h5>
						  </div>
						</figure>
					</div>

					<?php } ?>
				
				</div>
			</div>
		</div>
					 	
	</div>
</div>