<div class="go-green-background" style="background-image:url('resources/uielems/price_grid_details.png');">
	<div class="go-green-content">
		<table class="hide-on-small-only">
			<tr>
				<td style="text-align:right" data-aos="zoom-in">
					<img class="go-green-image" src="resources/uielems/travel_river_animation_ramotion.gif">
				</td>
				<td>
					<span data-aos="fade-left" class="go-green-topic">Go <span class="gone-green-text">Green.</span> Save Ceylon.</span>
					<p data-aos="fade-left" style="max-width:400px">When it comes to climate change, you must be the change you wish to see in the world. Join with us</p>

					<a href="gogreen"><button data-aos="zoom-out" class="blue-button">Learn more</button></a>
				</td>
			</tr>
		</table>

		<div class="hide-on-med-and-up">
			<div class="container">
				<img  data-aos="zoom-in" class="go-green-image" src="resources/uielems/travel_river_animation_ramotion.gif">

				<span  data-aos="fade-left" class="go-green-topic">Go <span class="gone-green-text">Green.</span> Save Ceylon.</span>
				<p  data-aos="fade-right" style="max-width:400px">When it comes to climate change, you must be the change you wish to see in the world. Join with us</p>

				<a href="gogreen"><button data-aos="zoom-out" class="blue-button">Learn more</button></a>
			</div>
		</div>
	</div>
</div>