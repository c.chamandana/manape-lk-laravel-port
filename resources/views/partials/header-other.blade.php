<header class="header">
	<div class="container">
		<div class="brand">
			<i class="mobile-menu-button fa-2x fa fa-bars"></i>
			<a href="/">
				<img class="brand-logo" src="resources/colorLogo.png">
			</a>
		</div>

		<div id="navbar-menu" class="menu">
			<i class="mobile-menu-close-button fa-2x fa fa-times"></i>

			<ul class="regular-menu">
				<a href="/#services"><li>Services</li></a>
				<a href="/#reviews"><li>Reviews</li></a>
				<a href="/#contact"><li>Contact</li></a>
			</ul>

			<ul class="account-menu">
				@if (Session::get("login") == "true")
					<a href="dashboard"><li class="waves-effect waves-dark signup-button">Go to Dashboard</li></a>
				@else
					<a href="login"><li class="login-button">Login</li></a>
					<a href="create"><li class="waves-effect waves-dark signup-button">Sign up</li></a>
				@endif
			</ul>
		</div>
	</div>
</header>