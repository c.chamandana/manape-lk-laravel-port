<footer class="footer">
	<div class="upperFooter">
		<div class="container">
			<a href="/"><img  class="brand-logo" src="resources/whiteLogo.png"></a>
			<img  class="brand-playstore-link" src="resources/account/App-Store-Google-Play-Badges-Vector.png">


			<div class="row">
				<div  class="col l4 m4 s12">
					<a href="/"><p>Home</p></a>
					<a href="#services"><p>Services</p></a>
					<a href="#reviews"><p>Reviews</p></a>
					<a href="#contact"><p>Contact</p></a>
				</div>
				<div class="col l4 m4 s12 center">
					<div class="non-center">
						<a href="/login"><p>Login</p></a>
						<a href="/create"><p>Sign up</p></a>
						<a href="/dashboard">Dashboard</a>
						<a href="#"><p>Support Center</p></a>
					</div>	
				</div>
				<div class="col l4 m4 s12 right">
					
				</div>
			</div>
		</div>
	</div>
	<div class="copyrights">
		<div class="container">
			<div class="copyrights-left">
				<span>Copyrights &copy; 2017 Manape.lk</span><br>
				<span>Manape.lk is a product of Zeonlab Digital Agency</span>
			</div>
			<div class="copyrights-right">
				<a href="/terms">Terms of Service</a>
				<a href="/privacy-policy">Privacy Policy</a>
			</div>
		</div>
	</div>
</footer>