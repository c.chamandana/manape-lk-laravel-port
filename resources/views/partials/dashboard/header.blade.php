  <ul id='dropdown1' class='dropdown-content' style="width:200px">
    <li><a class="waves-effect waves-dark" href="/dashboard">Dashboard</a></li>
    <li><a class="waves-effect waves-dark" href="/dashboard#settings">Account Settings</a></li>
    <li class="divider"></li>
    <li><a class="waves-effect waves-red" href="/signout">Sign Out</a></li>
  </ul>

<header class="dashboard-header-container">

	<div class="container dashboard-header">
		<a href="/"><img src="/resources/colorLogo.png" class="dashboard-header-logo"></a>

		<div class="right-aligned-user">
			<i id="notifications-button" title="Notifications" class="fa fa-bell-o notifications-icon"><div class="new"></div></i>

			<a href="#">
				<div class="user-information">
					<table class="hide-on-small-only dropdown-button" data-activates='dropdown1' style="margin-top:2px">
						<tr>
							@if (Session::get("user")->profile == "")
								<td style="padding-top:0"><img class="profile-picture" src="/resources/profile.jpg"></td>
							@else 
								<td style="padding-top:0"><img class="profile-picture" src="{{ Session::get("user")->profile }}"></td>
							@endif 

							<a>
								<td style="padding-top:0"><span class="username">{{ Session::get("user")->firstname }}</span></td>
								<td><i class="fa fa-chevron-down" style="transform:translateY(-8px)"></i></td>
							</a>
						</tr>
					</table>

					<div class="hide-on-med-and-up">
						<img class="profile-picture" src="/resources/profile.jpg">
					</div>
				</div>
			</a>
		</div>
	</div>

</header>	

