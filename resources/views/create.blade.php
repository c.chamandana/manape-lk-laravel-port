@extends('layouts.public')

@section('title', 'Join our community | Manape.lk')

@section('content')

	
<div class="container" style="margin-top:25px;margin-bottom:150px">
	<div class="row">
		<div class="col l6 m7 s12">
			<img src="resources/account/607734986.jpg" width="100%">
		</div>

		<div class="col l6 m5 s12">
			<div style="text-align:center">
				<p class="blue-subtext" style="font-size:20px;font-weight:bold">Join our Community</p>
				<p class="gray-subtext" style="font-size:13px;">Get unlimited access to<br>Sri Lanka's #1 online voting portal</p>

				{{-- <div class="large-space" style="margin-bottom:40px"></div>

				<p class="gray-subtext big">Continue with Social Media</p>

				<a href="user/login/facebook">
					<button style="margin-right:5px" class="social-login facebook waves-effect waves-light"><i class="fa fa-facebook"></i> Continue with Facebook</button>
					
				</a>
				<a href="user/login/google">
					<button class="social-login google-plus waves-effect waves-light"><i class="fa fa-google-plus"></i> Continue with Google+</button>
				</a>

				<div class="large-space" style="margin-bottom:40px"></div> --}}		

				{{-- <p class="gray-subtext big">Continue with email</p>	 --}}

				<div class="large-space" style="margin-bottom:30px"></div>		

				<form id="create-form" method="POST" action="/user/create" enctype="multipart/form-data" style="text-align:left">
					{{ csrf_field() }}
					<div class="row">
						<div class="col l6 m6 s12">
							<div class="input-field">
								<input name="firstname" placeholder="John" style="font-size:13px;padding-left:10px;padding-right:10px" id="firstname" type="text">
								<label style="padding-left:14px;padding-right:14px" for="firstname">First Name</label>
					        </div>
					    </div>
					    <div class="col l6 m6 s12">
							<div class="input-field">
								<input name="lastname" placeholder="Doe" style="font-size:13px;padding-left:10px;padding-right:10px" id="lastname" type="text">
								<label style="padding-left:14px;padding-right:14px" for="lastname">Last Name</label>
					        </div>
					    </div>
				    </div>

			        <div class="row">
						<div class="col l6 m6 s12">
							<div class="input-field">
								<input name="nic" placeholder="XXXXXXXXXX" style="font-size:13px;padding-left:10px;padding-right:10px" id="nic" type="text">
								<label style="padding-left:14px;padding-right:14px" for="nic">NIC Number</label>
					        </div>
					    </div>
					    <div class="col l6 m6 s12">
							<div class="input-field">
								<input name="contact" placeholder="+94 XX XXX XXXX" style="font-size:13px;padding-left:10px;padding-right:10px" id="contact" type="tel" >
								<label style="padding-left:14px;padding-right:14px" for="contact">Contact No</label>
					        </div>
					    </div>
				    </div>

			        <div class="input-field" style="margin-bottom:10px">
						<input name="email" placeholder="example@domain.com" style="font-size:13px;padding-left:10px;padding-right:10px" id="email" type="email">
						<label style="padding-left:14px;padding-right:14px" for="email">Email</label>
			        </div>

			        <div class="row">
						<div class="col l6 m6 s12">
							<div class="input-field" style="margin-bottom:10px">
								<input name="password" placeholder="" style="font-size:13px;padding-left:10px;padding-right:10px" id="password" type="password" >
								<label style="padding-left:14px;padding-right:14px" for="password">Password</label>
							</div>
					    </div>
					    <div class="col l6 m6 s12">
					        <div class="input-field" style="margin-bottom:10px">
								<input name="confirm" placeholder="" style="font-size:13px;padding-left:10px;padding-right:10px" id="confirm-password" type="password" >
								<label style="padding-left:14px;padding-right:14px" for="confirm-password">Confirm Password</label>
					        </div>
					    </div>
				    </div>

				    <div class="password-validation">
				    	<p id="password-length-error">
				    		<i class="fa fa-times"></i> Password should be more than 6 characters long
				    	</p>
				    	<p id="password-numbers-error">
				    		<i class="fa fa-times"></i> Password should contain at least one Number
				    	</p>
				    </div>

				    <p style="padding:0 10px;margin-bottom:5px">
						<input type="checkbox" class="filled-in" id="tacs" />
						<label style="font-size:13px;" for="tacs">I agree to the <a href="/terms">Terms and Conditions of Service of Manape.lk</a></label>
				    </p>
				    <p style="padding:0 10px;margin-bottom:25px">
						<input type="checkbox" class="filled-in" id="newsletter" />
						<label style="font-size:13px;" for="newsletter">Email me News and updates related elections I voted</label>
				    </p>

			        <button id="signup-button" style="display:inline;margin-left:10px;padding-left:25px;padding-right:25px" class="blue-button waves-effect waves-dark">Sign Up</button>

			        <a href="login"><span class="create-account">Already a member</span></a>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')

	<script type="text/javascript" src="scripts/create.js"></script>

	@if(Session::has("message-class") && Session::has("message"))
	<script type="text/javascript">
		Materialize.toast('{{ Session::get("message") }}', 8000, '{{ Session::get("message-class") }}');
	</script>
	@endif

@endsection

