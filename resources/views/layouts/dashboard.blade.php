<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>

	<meta name="theme-color" content="#216ddd" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/resources/favicon.png">
	<link rel="stylesheet" type="text/css" href="/bower_components/materialize/dist/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/font-awesome/css/font-awesome.min.css"> 
	<link rel="stylesheet" type="text/css" href="/bower_components/aos/dist/aos.css">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

	<link rel="stylesheet" href="/bower_components/OwlCarousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="/bower_components/OwlCarousel/dist/assets/owl.theme.default.min.css">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<!-- Include Editor style. -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />


	<link rel="stylesheet" type="text/css" href="/bower_components/lightbox2/dist/css/lightbox.min.css">
	<link href="http://cdn.quilljs.com/1.3.5/quill.snow.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="/css/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="/scripts/ccrouter/ccrouter.css">
</head>

<body class="gray-body">

	@include('partials.dashboard.header')

	@yield('content')
	
	<script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/bower_components/handlebars/handlebars.js"></script>
	<script type="text/javascript" src="/bower_components/materialize/dist/js/materialize.min.js"></script>
	<script type="text/javascript" src="/bower_components/aos/dist/aos.js"></script>
	<script src="/bower_components/OwlCarousel/dist/owl.carousel.min.js"></script>
	<script type="text/javascript" src="/bower_components/lightbox2/dist/js/lightbox.min.js"></script>
	<script type="text/javascript" src="/bower_components/moment/min/moment.min.js"></script>
	<script type="text/javascript" src="/bower_components/chart.js/dist/Chart.min.js"></script>
	<script type="text/javascript" src="/bower_components/web-animations-js/web-animations.min.js"></script>
	<script type="text/javascript" src="/bower_components/hammerjs/hammer.min.js"></script>
	<script type="text/javascript" src="/bower_components/muuri/muuri.min.js"></script>
	<script type="text/javascript" src="/scripts/index.js"></script>

	@if(Session::has("message-class") && Session::has("message"))
		<script type="text/javascript">
			Materialize.toast('{{ Session::get("message") }}', 8000, '{{ Session::get("message-class") }}');
		</script>
	@endif

	<script type="text/javascript">
		// Add the essential prototypes
		String.prototype.replaceAll = function(search, replacement) {
		    var target = this;
		    return target.split(search).join(replacement);
		};
	</script>

	@yield('scripts')
</body>
</html>