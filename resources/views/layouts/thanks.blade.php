<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="theme-color" content="#216ddd" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="resources/favicon.png">
	<link rel="stylesheet" type="text/css" href="/bower_components/materialize/dist/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/font-awesome/css/font-awesome.min.css"> 
	<link rel="stylesheet" type="text/css" href="/bower_components/aos/dist/aos.css">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

	<link rel="stylesheet" href="/bower_components/OwlCarousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="/bower_components/OwlCarousel/dist/assets/owl.theme.default.min.css">

	<link rel="stylesheet" type="text/css" href="/bower_components/lightbox2/dist/css/lightbox.min.css">

	<link rel="stylesheet" type="text/css" href="/css/stylesheet.css">
</head>

<body>

	@include('partials.dashboard.header')

	@yield('content')

	<script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/bower_components/materialize/dist/js/materialize.min.js"></script>
	<script type="text/javascript" src="/bower_components/aos/dist/aos.js"></script>
	<script src="/bower_components/OwlCarousel/dist/owl.carousel.min.js"></script>
	<script type="text/javascript" src="/bower_components/lightbox2/dist/js/lightbox.min.js"></script>
	
	<script type="text/javascript" src="/scripts/index.js"></script>
	<script type="text/javascript" src="/scripts/common.js"></script>

	@yield('scripts')
</body>
</html>