@extends('layouts.public')

@section('title', 'Login to your account | Manape.lk')

@section('content')

<div class="container" style="margin-top:50px;margin-bottom:100px">
	<div class="row">
		<div class="col l6 m7 s12">
			<img src="resources/account/background.jpg" width="100%">
		</div>

		<div class="col l6 m5 s12">
			<div style="text-align:center">
				<p class="blue-subtext" style="font-size:20px;font-weight:bold">Have an account?</p>
				<p class="gray-subtext" style="font-size:13px;">Welcome. Please login to your account to access to the voting Portal</p>

				{{-- <div class="large-space" style="margin-bottom:45px"></div> --}}

				{{-- <p class="gray-subtext big">Continue with Social Media</p>

				<a href="user/login/facebook">
					<button style="margin-right:5px" class="social-login facebook waves-effect waves-light"><i class="fa fa-facebook"></i> Continue with Facebook</button>
				</a>
				<a href="user/login/google">
					<button class="social-login google-plus waves-effect waves-light"><i class="fa fa-google-plus"></i> Continue with Google+</button>
				</a>

				<div class="large-space" style="margin-bottom:50px"></div>		 --}}

				{{-- <p class="gray-subtext big">Continue with email</p>	 --}}

				<div class="large-space" style="margin-bottom:50px"></div>

				<form id="login-form" enctype="multipart/form-data" method="POST" action="user/login" style="text-align:left">
					{{ csrf_field() }}
					<div class="input-field">
						<input name="email" style="font-size:13px;padding-left:10px;padding-right:10px" placeholder="" id="email" type="email">
						<label style="padding-left:14px;padding-right:14px" for="email">Email</label>
			        </div>
			        <div class="input-field" style="margin-bottom:10px">
						<input name="password" style="font-size:13px;padding-left:10px;padding-right:10px"  placeholder="" id="password" type="password">
						<label style="padding-left:14px;padding-right:14px" for="password">Password</label>
			        </div>

			        <p style="padding:0 10px;margin-bottom:25px">
						<input name="remember" type="checkbox" class="filled-in" id="remember" />
						<label style="font-size:13px;" for="remember">Remember this browser</label>
				    </p>

			        <button id="login-button" style="display:inline;margin-left:10px;padding-left:25px;padding-right:25px" class="blue-button waves-effect waves-dark">Login</button>

			        <a href="create"><span class="create-account">Create an account</span></a>
			        <a href="forgot"><span class="forgot-password">Forgot password?</span></a>
				</form>
			</div>
		</div>
	</div>
</div>

@if (Session::get("login") == "true")
  <script type="text/javascript">window.location = "dashboard/";</script>
@endif

@endsection

@section('scripts')
<script type="text/javascript" src="scripts/login.js"></script>

@if(Session::has("message-class") && Session::has("message"))
	<script type="text/javascript">
		Materialize.toast('{{ Session::get("message") }}', 8000, '{{ Session::get("message-class") }}');
	</script>
@endif
@endsection