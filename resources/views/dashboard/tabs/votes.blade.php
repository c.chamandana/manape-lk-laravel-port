<div class="dashboard-section">
	<div class="section-title">
		<span>Your votes</span>
	</div>

	@if (sizeof(Session::get("votes")) > 0)
		<div class="row">
			@foreach(Session::get("votes") as $vote)
			<a href="/vote/{{ $vote["election"]->id }}">
				<div  class="col l4 m6 s12">
					<div class="waves-effect waves-dark card vote-card hoverable">
						<?php $data = ($vote["data"]["options"]); ?>
						<?php $index = ($vote["vote"]->vote); ?>

						<span class="election-name truncate">{{ $vote["election"]->title }}</span>
						<span class="time-duration">Voted for <b>{{ $data[$index]["title"] }}</b></span>

						<span class="status update">{{ $vote["election"]->status }}</span>
					</div>	
				</div>
			</a>
			@endforeach
		</div>
	@else 
		You haven't voted on any election
	@endif
</div>