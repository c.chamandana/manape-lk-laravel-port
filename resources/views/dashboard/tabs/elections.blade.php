<div id="createModal" class="modal modal-fixed-footer">	
	<form id="createModalForm" method="POST" action="/user/election/create" enctype="multipart/form-data" style="margin-top:50px;">
		{{csrf_field()}}
		<div class="note" style="font-size:20px;text-transform:uppercase;font-weight:bold">CREATE A NEW ELECTION</div>
		<div class="modal-content">
				<div class="input-field">
					<input data-length="100" name="title" placeholder="Board of Directors" style="font-size:13px;" id="title" type="text">
					<label style="padding-left:14px;padding-right:14px" for="title">Election Title</label>
		        </div>
	         	<div class="input-field">
					<textarea  data-length="1000" name="description" placeholder="A brief description of your election" id="description" class="materialize-textarea"></textarea>
					<label for="description">Election Description</label>
		        </div>
		        <div class="row">
		        	<div class="col l6 m6 s12">
		        		<div class="input-field">
							<input name="startdate" placeholder="The date the election starts" id="startdate" type="text" class="datepicker">
							<label style="padding-left:14px;padding-right:14px;" for="startdate">Start Date</label>
						</div>
		        	</div>
		        	<div class="col l6 m6 s12">
		        		<div class="input-field">
							<input name="enddate" placeholder="The date the election ends" id="enddate" type="text" class="datepicker">
							<label style="padding-left:14px;padding-right:14px;" for="enddate">End Date</label>
						</div>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col l6 m6 s12">
		        		<div class="input-field">
							<input name="starttime" placeholder="The time the election starts" id="starttime" type="text" class="timepicker">
							<label style="padding-left:14px;padding-right:14px;" for="starttime">Start Time</label>
						</div>
		        	</div>
		        	<div class="col l6 m6 s12">
		        		<div class="input-field">
							<input name="endtime" placeholder="The time the election ends" id="endtime" type="text" class="timepicker">
							<label style="padding-left:14px;padding-right:14px;" for="endtime">End Time</label>
						</div>
		        	</div>
		        </div>

		        <span class="notice">Note that Manape.lk uses <b>Sri Jayawardhenapura (+5.30)</b> timezone for all of it's time calculations</span>
		
		</div>
		<div class="modal-footer">
			<a id="createElectionButton" class="waves-effect waves-dark btn-flat ">Create</a>
		</div>

	</form>
</div>


<div class="dashboard-section">
	<div class="section-title">
		<span>Your elections</span>
	</div>

	<div class="row">
		@if(sizeof(Session::get("elections")) == 0)

		@else
			@foreach ((Session::get("elections")["draft"]) as $election)
			
					<a href="/dashboard/election/{{ $election->id }}">
						<div  class="col l4 m6 s12">
							<div class="waves-effect waves-dark card election-card hoverable">
								<span class="election-name truncate">{{$election->title}}</span>
								<span class="time-duration">{{$election->startdate}} - {{$election->enddate}}</span>

								<span class="status {{ strtolower($election->status) }}">{{ $election->status}}</span>
							</div>	
						</div>
					</a>
			@endforeach
			@foreach ((Session::get("elections")["active"]) as $election)
			
					<a href="/dashboard/election/{{ $election->id }}">
						<div  class="col l4 m6 s12">
							<div class="waves-effect waves-dark card election-card hoverable">
								<span class="election-name truncate">{{$election->title}}</span>
								<span class="time-duration">{{$election->startdate}} - {{$election->enddate}}</span>

								<span class="status {{ strtolower($election->status) }}">ACTIVE</span>
							</div>	
						</div>
					</a>
			@endforeach
			@foreach ((Session::get("elections")["live"]) as $election)
			
					<a href="/dashboard/election/{{ $election->id }}">
						<div  class="col l4 m6 s12">
							<div class="waves-effect waves-dark card election-card hoverable">
								<span class="election-name truncate">{{$election->title}}</span>
								<span class="time-duration">{{$election->startdate}} - {{$election->enddate}}</span>

								<span class="status {{ strtolower($election->status) }}">LIVE</span>
							</div>	
						</div>
					</a>
			@endforeach
		@endif

		<a onclick="openCreateModal();">
			<div class="col l4 m6 s12">
				<div class="waves-effect waves-dark election-add-card">
					<div class="content">
						<i class="fa fa-plus"></i> Create a new election
					</div>
				</div>
			</div>
		</a>
	</div>
</div>

<div class="dashboard-section">
	<div class="section-title">
		<span>Ended Elections</span>
	</div>

	<div class="row">
		@if(sizeof(Session::get("elections")) == 0)

		@else
			@foreach ((Session::get("elections")["ended"]) as $election)
			
					<a href="/dashboard/election/{{ $election->id }}">
						<div  class="col l4 m6 s12">
							<div class="waves-effect waves-dark card election-card hoverable">
								<span class="election-name truncate">{{$election->title}}</span>
								<span class="time-duration">{{$election->startdate}} - {{$election->enddate}}</span>

								<span class="status ended">ENDED</span>
							</div>	
						</div>
					</a>
			@endforeach
		@endif
	</div>
</div>