<div class="dashboard-section">
	<div class="row">
		<div class="col l6 m6 s12">
				
			<div class="section-title">
				<span>Account Settings</span>
			</div>

			<div class="card hoverable">
				<ul class="collection">

					<a href="#">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Change password</span>
										<span class="setting-description">Change your password you use to login to Manape.lk</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a href="#">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Verify Account</span>
										<span class="setting-description">Verify your Email and NIC number</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

			    </ul>
			</div>
				
			<div class="section-title" style="margin-top:50px">
				<span>Profile Settings</span>
			</div>

			<div class="card hoverable setting-card" style="padding-top:75px">
				<form id="organization_settings" method="POST" action="/settings/save/user" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="note">You need to verify your Email address and NIC number in order to host an election or vote. Click &quot;Verify&quot; under Account Settings section to continue</div>

					<div style="padding-left:0; padding-right:0" class="input-field">
						<input name="firstname" value="{{ Session::get('user')->firstname }}" style="font-size:13px" placeholder="John" id="firstname" type="text">
						<label for="firstname">First name</label>
			        </div>
			        <div style="padding-left:0; padding-right:0" class="input-field">
						<input name="lastname" value="{{ Session::get('user')->lastname }}" style="font-size:13px" placeholder="Doe" id="lastname" type="text">
						<label for="lastname">Last name</label>
			        </div>
			        <div style="padding-left:0; padding-right:0" class="input-field">
						<input name="nic" value="{{ Session::get('user')->nic }}" style="font-size:13px;" placeholder="XXXXXXXXX" id="nic" type="text">
						<label for="nic">NIC Number</label>
			        </div>
			         <div style="padding-left:0; padding-right:0" class="input-field">
						<input name="contact" value="{{ Session::get('user')->contact }}" style="font-size:13px;margin-bottom:10px" placeholder="0XX XXX XXXX" id="contact" type="tel">
						<label for="contact">Contact number</label>
			        </div>

			        <button style="margin-top:15px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Profile Settings</button>
				</form>
			</div>

		</div>

		<div class="col l6 m6 s12">
				
			<div class="section-title">
				<span>Organization Settings</span>
			</div>

			<div class="card hoverable setting-card" style="padding-top:75px">
				<form id="organization_settings" method="POST" action="/settings/save/organization" enctype="multipart/form-data">
					{{ csrf_field() }}

					@if (Session::has("organization"))
						<div class="note">When you host an election your organization information is shown instead of your profile information.</div>
						
						<div style="padding-left:0; padding-right:0" class="input-field">
							<input value="{{ Session::get("organization")->name }}" name="org_name" style="font-size:13px" placeholder="ACME Org." id="org_name" type="text">
							<label for="org_name">Organization Name</label>
				        </div>
				        <div style="padding-left:0; padding-right:0" class="input-field">
							<input value="{{ Session::get("organization")->location }}" name="org_address" style="font-size:13px" placeholder="13/23, Lorem Ipsum Avenue, Colombo 7" id="org_address" type="text">
							<label for="org_address">Organization Location</label>
				        </div>
				        <div style="padding-left:0; padding-right:0" class="input-field">
							<input value="{{ Session::get("organization")->handle }}" name="org_handle" style="font-size:13px;margin-bottom:10px" placeholder="acmeinc" id="org_id" type="text">
							<label for="org_id">Organization Identifier / Handle</label>
				        </div>

				        <p style="margin-bottom:5px">
							<input name="org_searchable" type="checkbox" class="filled-in" id="tacs" />
							<label style="font-size:13px;" for="tacs">Make organization searchable</label>
					    </p>

					    <button style="margin-top:18px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Organization Settings</button>
					@else 
						<div class="note">When you host an election your organization information is shown instead of your profile information.</div>
						
						<div style="padding-left:0; padding-right:0" class="input-field">
							<input name="org_name" style="font-size:13px" placeholder="ACME Org." id="org_name" type="text">
							<label for="org_name">Organization Name</label>
				        </div>
				        <div style="padding-left:0; padding-right:0" class="input-field">
							<input name="org_address" style="font-size:13px" placeholder="13/23, Lorem Ipsum Avenue, Colombo 7" id="org_address" type="text">
							<label for="org_address">Organization Location</label>
				        </div>
				        <div style="padding-left:0; padding-right:0" class="input-field">
							<input name="org_handle" style="font-size:13px;margin-bottom:10px" placeholder="acmeinc" id="org_id" type="text">
							<label for="org_id">Organization Identifier / Handle</label>
				        </div>

				        <p style="margin-bottom:5px">
							<input name="org_searchable" type="checkbox" class="filled-in" id="tacs" />
							<label style="font-size:13px;" for="tacs">Make organization searchable</label>
					    </p>

					    <button style="margin-top:18px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Organization Settings</button>
					@endif
				</form>
			</div>

			<div class="section-title" style="margin-top:50px;">
				<span>Appearance Settings</span>
			</div>

			<div class="card hoverable setting-card" style="padding-top:45px">
				<form id="organization_settings" method="POST" action="/settings/save/appearance" enctype="multipart/form-data">
					{{ csrf_field() }}

					<div class="note">Change organization appearance to personalize the user experience your voters get</div>

					<div style="padding-left:0; padding-right:0" class="input-field">
						<span style="display:block;font-size:12px; color:#9e9e9e;margin-bottom:5px;">Organization Logo</span>
						<input style="font-size:13px;" id="org_logo" type="file" name="Organization Logo">
			        </div>
			        <div style="padding-left:0; padding-right:0" class="input-field">
						<span style="display:block;font-size:12px; color:#9e9e9e;margin-bottom:5px;">Accent Color</span>

						<div class="colors">
							<div class="color selected" style="background-color:#216ddd;color:white;" data-color="dodgerBlue" data-name="Dodger Blue"></div>
							<div class="color" style="background-color:red;color:white" data-color="red" data-name="Red"></div>
							<div class="color" style="background-color:orangered;color:white" data-color="orangered" data-name="Orange Red"></div>
							<div class="color" style="background-color:orange;color:white" data-color="orange" data-name="Orange"></div>
							<div class="color" style="background-color:green;color:white;" data-color="green" data-name="Green"></div>
							<div class="color" style="background-color:purple;color:white" data-color="purple" data-name="Purple"></div>
						</div>
			        </div>

				    <button style="margin-top:18px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Appearance Settings</button>
				</form>
			</div>

		</div>

	</div>
</div>