<div class="location">
	<a class="backlink" href="/dashboard"><i class="fa fa-chevron-left"></i> Dashboard</a><span class="slash">/</span> <span class="current">{{ Session::get("election")->title }}</span>
</div>

<div id="delete-confirm" class="modal">
	<div class="modal-content">
		<h4>Election Delete Confirmation</h4>
		<p>Are you sure you want to delte election <strong>{{ Session::get("election")->title }}</strong></p>
	</div>
	<div class="modal-footer">
		<a href="/settings/election/delete/{{ Session::get("election")->id }}" class="modal-action red-text modal-close waves-effect waves-red btn-flat ">Yes</a>
		<a class="modal-action modal-close waves-effect waves-dark btn-flat ">No</a>
	</div>
</div>

<div class="dashboard-section">
	<div class="row">
		<div class="col l6 m6 s12">
				
			<div class="section-title">
				<span>Election Details</span>
			</div>

			<div class="card hoverable setting-card" style="padding-top:75px; margin-bottom:25px">
				<form id="election_settings" method="POST" enctype="multipart/form-data">
					<meta name="csrf-token" content="{{ csrf_token() }}">

					<div class="note">These details are displayed for every user who visits your election through your link, so keep the text tidy</div>
					
					<div style="padding-left:0; padding-right:0" class="input-field">
						<input id="election_title" data-length="100" name="election_title" value="{{ Session::get('election')->title }}" style="font-size:13px;margin-bottom:0 !important" placeholder="Chairman of Chess Club" id="election_title" type="text">
						<label for="election_title">Election Title</label>
			        </div>

			        <div style="padding-left:0; padding-right:0" class="input-field">
			        	<p style="font-size:12px;color:#777;">Election Description</p>
						<div id="toolbar" class="sc-toolbar-container toolbar">
							<div class="sc-format-group">
								<span class="sc-bold sc-format-button"></span>
								<span class="sc-italic sc-format-button"></span>
								<span class="sc-strike sc-format-button"></span>
								<span class="sc-underline sc-format-button"></span>
								<span class="sc-link sc-format-button"></span>
								<span class="sc-format-separator"></span>
								<select class="sc-size">
									<option value="small">Small</option>
									<option value="normal">Normal</option>
									<option value="large">Large</option>
								</select>
							</div>

							<div id="editor">
								
							</div>
						</div>

			        </div>

				    <button id="election-settings-save-btn" style="margin-top:18px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Election Details</button>
				</form>
			</div>


			<div class="section-title">
				<span>Election Dates</span>
			</div>

			<div class="card hoverable setting-card" style="padding-top:60px">
				<form id="election_dates" method="POST" enctype="multipart/form-data">
					<meta name="csrf-token" content="{{ csrf_token() }}">

					<div class="note">All election dates are based on Sri Jayawardhenapura (+5:30) timezone. Elections wont start automatically until you launch them</div>
					

			        <div class="row" style="margin-top:20px;margin-bottom:20px;">
			        	<div class="col l6 m6 s12">
			        		<div class="input-field" style="padding-left: 0; padding-right:0">
								<input name="startdate" style="margin-bottom:0" name="startdate" placeholder="The date the election starts" id="startdate" type="text" class="datepicker">
								<label style="padding-left:0;padding-right:14px;" for="startdate">Start Date</label>
							</div>
			        	</div>
			        	<div class="col l6 m6 s12" style="padding-left: 0; padding-right:0">
			        		<div class="input-field">
								<input name="enddate" style="margin-bottom:0" name="enddate" placeholder="The date the election ends" id="enddate" type="text" class="datepicker">
								<label style="padding-left:14px;padding-right:14px;" for="enddate">End Date</label>
							</div>
			        	</div>
			        </div>
			        <div class="row">
			        	<div class="col l6 m6 s12" style="padding-left: 0; padding-right:0">
			        		<div class="input-field">
								<input name="starttime" style="margin-bottom:0" name="starttime" placeholder="The time the election starts" id="starttime" type="text" class="timepicker">
								<label style="padding-left:14px;padding-right:14px;" for="starttime">Start Time</label>
							</div>
			        	</div>
			        	<div class="col l6 m6 s12" style="padding-left: 0; padding-right:0">
			        		<div class="input-field">
								<input name="endtime" style="margin-bottom:0" name="endtime" placeholder="The time the election ends" id="endtime" type="text" class="timepicker">
								<label style="padding-left:14px;padding-right:14px;" for="endtime">End Time</label>
							</div>
			        	</div>
			        </div>

				    <button id="save-election-timing-button" style="margin-top:18px;margin-bottom:10px;" class="blue-button waves-effect waves-dark">Save Election Dates</button>
				</form>
			</div>

		</div>

		<div class="col l6 m6 s12">

			<div class="section-title">
				<span>Election Options</span>
			</div>

			<div class="card hoverable" style="margin-bottom:25px;">
				<ul class="collection">

					<a href="#options">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Create options</span>
										<span class="setting-description">Create options for the voters to select</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a href="/dashboard#settings">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Update Appearance <i style="margin-left:10px;" class="fa fa-external-link"></i></span>
										<span class="setting-description">Update organization information and appearance</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

			    </ul>
			</div>


			<div class="section-title">
				<span>Election Ownership</span>
			</div>
 
			<div class="card hoverable" style="border:none;">
				<ul class="collection">

					<a href="#">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Transfer Ownership</span>
										<span class="setting-description">Change ownership of the current election</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a href="#">
						<li class="waves-effect waves-dark collection-item">
							<table>
								<tr>
									<td>
										<span class="setting">Duplicate Election</span>
										<span class="setting-description">Create another election with same details and options</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a id="delete-election-button">
						<li class="waves-effect waves-dark collection-item" style="background-color:#f33;;">
							<table>
								<tr>
									<td>
										<span style="color:white !important" class="setting">Delete Election</span>
										<span style="color:white !important" class="setting-description">Delete the election from your drafts</span>
									</td>
									<td>
										<a href="#!" class="secondary-content"><i style="color:white" class="fa fa-chevron-right"></i></a>
									</td>
								</tr>
							</table>
						</li>
					</a>

			    </ul>
			</div>

		</div>

	</div>
</div>