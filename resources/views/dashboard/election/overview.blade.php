<div class="location">
	<a class="backlink" href="/dashboard"><i class="fa fa-chevron-left"></i> Dashboard</a><span class="slash">/</span> <span class="current">{{ Session::get("election")->title }}</span>
</div>

<div class="progress-tracker card hoverable">
	<div class="progress-text">
		<?php
		$percentage_score = 0;

		if (Session::get("launch-steps")["options"])
			$percentage_score += 33;

		if (Session::get("launch-steps")["organization"])
			$percentage_score += 33;

		if (Session::get("election")->status == "LIVE")
			$percentage_score += 33;
		?>

		@if($percentage_score == 99)
			100% Completed
		@else
			{{ $percentage_score }}% Completed
		@endif
	</div>
	<table>
		<tr>
			
			@if (Session::get("launch-steps")["options"])
				<td class="complete">
			@else
				<td>
			@endif
				<a href="#options">
					<i class="fa"></i> Build your options
				</a>
			</td>
		
			@if (Session::get("launch-steps")["organization"])
				<td class="complete"> 
			@else
				<td>
			@endif
				<a href="/dashboard#settings">
					<i class="fa"></i> Configure your settings				
				</a>
			</td>
		
			@if (Session::get("election")->status == "LIVE")
				<td class="complete">
			@else
				<td>
			@endif
				<a href="#launch">
					<i class="fa"></i> Launch your election
				</a>
			</td>
			
		</tr>
	</table>
</div>

<div class="row">
	<div class="col l6 m6 s12">
		<div class="card time-card hoverable" style="margin-bottom:25px">
			<div class="row" style="margin-bottom: 0 !important">
				<a href="#">
					<div class="col l6 m6 s6 right-bordered">
						<div class="title">Start Date</div>
						<span class="time">{{ Session::get("election")->startdate }}</span>
					</div>
				</a>
				<a href="#">
					<div class="col l6 m6 s6">
						<div class="title">End Date</div>
						<span class="time">{{ Session::get("election")->enddate }}</span>
					</div>
				</a>
			</div>
		</div>

		@if (Session::get("election")->status == "DRAFT")
			<div class="card basic-analytics-card hoverable" style="padding:15px 25px">
				<div class="centered">
					<span class="title">Analytics are not available</span>
					<span class="advice">Launch your election to see Analytics</span>
				</div>
				<canvas id="myChart" width="400px" height="250px" style="filter: blur(25px);pointer-events:none;height:400px !important"></canvas>
			</div>
		@else 
			<div class="card basic-analytics-card hoverable" style="padding:20px 25px;text-align:center;">
				<canvas id="votes_days" width="400px" height="250px" style="height:400px !important"></canvas>
				<a href="#analytics" class="waves-effect waves-dark blue-button" style="display:inline-block;text-align:center;margin-top:25px;margin-bottom:10px">VIEW ANALYTICS</a>
				<a href="#settings" class="waves-effect waves-dark blue-button" style="margin-left:5px;display:inline-block;text-align:center;margin-top:25px;margin-bottom:10px">CHANGE SETTINGS</a>
			</div>
		@endif
	</div>

	<div class="col l6 m6 s12">
		<div class="card hoverable links-card setting-card" style="padding-top:50px;padding-bottom:20px;margin-bottom:25px">
			<form>

				<div class="note">
					Share this election. Spread the word
				</div>

				<table style="margin-bottom:25px">
					<tr>
						<td  style="width:125px;font-weight:bold">
							Direct URL
						</td>
						<td>
							<input type="text" name="directurl" id="directurl" value="http://manape.lk/vote/{{ Session::get("election")->id }}">
						</td>
					</tr>
					<tr>
						<td  style="width:125px;font-weight:bold">
							Shortened URL
						</td>
						<td>
							<input type="text" name="directurl" id="directurl" value="http://mnp.lk/172839">
						</td>
					</tr>
				</table>

				<div class="social-share">
					<?php 

					$url = "http://www.manape.lk/vote/" . Session::get("election")->id; 
					$page_title = Session::get("election")->title . ": vote now on Manape.lk";

					?>

					<span class="small-title">SHARE ON SOCIAL MEDIA</span>
					<a href="https://www.facebook.com/dialog/share?app_id=1567880006630608&display=page&href={{ $url }}" target="_blank" title="Share on Facebook">
						<div class="waves-effect waves-dark social-share-button facebook">
							<i class="fa fa-facebook"></i>
						</div>
					</a>
					<a href="https://plus.google.com/share?url={{ $url }}" target="_blank" title="Share on Google Plus">
						<div class="waves-effect waves-dark social-share-button google-plus">
							<i class="fa fa-google-plus"></i>
						</div>
					</a>
					<a href="https://twitter.com/intent/tweet?url={{ $url }}&text={{ $page_title }}" target="_blank" title="Tweet this election">
						<div class="waves-effect waves-dark social-share-button twitter">
							<i class="fa fa-twitter"></i>
						</div>
					</a>
					<a href="https://pinterest.com/pin/create/bookmarklet/?url={{ $url }}&is_video=false&description={{ $page_title }}" target="_blank" title="Share on Pinterest">
						<div class="waves-effect waves-dark social-share-button pinterest">
							<i class="fa fa-pinterest"></i>
						</div>
					</a>
					<a href="https://www.linkedin.com/shareArticle?url={{ $url }}&title={{ $page_title }}" target="_blank" title="Share on LinkedIn">
						<div class="waves-effect waves-dark social-share-button linkedin">
							<i class="fa fa-linkedin"></i>
						</div>
					</a>

					<a href="#" title="Copy link to clipboard">
						<div class="waves-effect waves-dark social-share-button link">
							<i class="fa fa-link"></i>
						</div>
					</a>
				</div>

			</form>
		</div>

		<div class="hoverable card counter-card">
			<div class="row">
				<div class="col l4 m4 s4 right-bordered">
					<div class="number">{{ sizeof(Session::get("election-data")["options"]) }}</div>
					<div class="text">Options</div>
				</div>
				<div class="col l4 m4 s4 right-bordered">
					<div class="number">{{ Session::get("categorized-votes")["total"] }}</div>
					<div class="text">Votes</div>
				</div>
				<div class="col l4 m4 s4">
					<div class="number">{{ Session::get("election")->visits }}</div>
					<div class="text">Visits</div>
				</div>
			</div>
		</div>
	</div>
</div>