<div class="location">
	<a class="backlink" href="/dashboard"><i class="fa fa-chevron-left"></i> Dashboard</a><span class="slash">/</span> <span class="current">{{ Session::get("election")->title }}</span>
</div>

@if (Session::get("election")->status == "DRAFT")

	<div class="no-analytics">
		<span class="big-text">Sorry</span>
		<span>Analytics are only available when the election is launched</span>
	</div>

@else 
	<div class="row">
		<div class="col l5 m6 s12">
			<div class="card hoverable analytics-card">
				<form>
					<div class="note">Votes obtained by candidates / options</div>
				</form>

				<canvas id="votes-chart" width="400px" height="250px" style="height:300px !important"></canvas>
			</div>

			<div class="hoverable card counter-card">
				<div class="row">
					<div class="col l4 m4 s4 right-bordered">
						<div class="number">{{ sizeof(Session::get("election-data")["options"]) }}</div>
						<div class="text">Options</div>
					</div>
					<div class="col l4 m4 s4 right-bordered">
						<div class="number">{{ Session::get("categorized-votes")["total"] }}</div>
						<div class="text">Votes</div>
					</div>
					<div class="col l4 m4 s4">
						<div class="number">{{ Session::get("election")->visits }}</div>
						<div class="text">Visits</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col l7 m6 s12">
			<div class="card hoverable analytics-card">
				<form>
					<div class="note">Votes based on days</div>
				</form>

				<canvas id="vote-time-graph" width="400px" height="250px" style="height:300px !important"></canvas>
			</div>
		</div>
	</div>
	

@endif