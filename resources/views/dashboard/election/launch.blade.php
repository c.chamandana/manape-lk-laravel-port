<div class="location">
	<a class="backlink" href="/dashboard"><i class="fa fa-chevron-left"></i> Dashboard</a><span class="slash">/</span> <span class="current">{{ Session::get("election")->title }}</span>
</div>

<div class="dashboard-section">
	<div class="section-title" style="margin-bottom:40px;">
		<span>Launch Election</span>
	</div>

	<div class="row">
		<div class="col l8 m12 s12">
			<span class="subsection-title">Launch Checklist : </span>

			<div class="launch-checklist card">
				<ul class="collection">

					<a href="/dashboard#settings">
						<li class="collection-item waves-effect waves-dark">
							<table>
								<tr>
									<td style="width:50px">
										@if (Session::get("launch-steps")["organization"] == true)
											<i class="fa fa-circle fa-2x"></i>
										@else
											<i class="fa fa-circle-o fa-2x"></i>
										@endif
									</td>
									<td>
										<b>Provide your organization details</b><br>
										Your organization details including organization name, address and telephone number are required
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a href="#settings">
						<li class="collection-item waves-effect waves-dark">
							<table>
								<tr>
									<td style="width:50px">
										<i class="fa fa-circle fa-2x"></i>
									</td>
									<td>
										<b>Fill all information about the election</b><br>
										Election title, description, start data, start time should be supplied in order to launch your election
									</td>
								</tr>
							</table>
						</li>
					</a>

					<a href="#options">
						<li class="collection-item waves-effect waves-dark">
							<table>
								<tr>
									<td style="width:50px">
										@if (Session::get("launch-steps")["options"] == true)
											<i class="fa fa-circle fa-2x"></i>
										@else
											<i class="fa fa-circle-o fa-2x"></i>
										@endif
									</td>
									<td>
										<b>Create options for your election</b><br>
										Options are for users / voters to select when they are voting and are essential for an election
									</td>
								</tr>
							</table>
						</li>
					</a>

				</ul>
			</div>
		</div>

		<div class="col l4 m12 s12">
			<span class="subsection-title">Launch Status : </span>

			@if (Session::get("election")->status == "DRAFT")

				@if (Session::get("launch-steps")["organization"] && Session::get("launch-steps")["options"])
					<blockquote class="launch-note">
						You're good to go. You can launch your election now
					</blockquote>
				@else
					<blockquote class="launch-note">
						In order to launch your election you need to fullfill some essentials first.
					</blockquote>
				@endif


				<span class="subsection-title" style="margin-top:30px">Launch Election : </span>

				@if (Session::get("launch-steps")["organization"] && Session::get("launch-steps")["options"])
					<a href="/settings/election/launch/{{ Session::get("election")->id }}" class="launch-button waves-effect waves-dark">
						<i class="fa fa-rocket"></i> Launch Election
					</a>
				@else
					<a class="launch-button disabled waves-effect waves-dark">
						<i class="fa fa-rocket"></i> Launch Election
					</a>
				@endif

			@else

				<blockquote class="launch-note">
					This election is already launched and available for public
				</blockquote>

				<span class="subsection-title" style="margin-top:30px">Launch Election : </span>

				<a href="/settings/election/abort/{{ Session::get("election")->id }}" class="launch-button waves-effect waves-dark">
					<i class="fa fa-times"></i> Abort Election
				</a>

			@endif

		</div>
	</div>


</div>