<script id="item-template" type="text/javascript" src="scripts/option-card.hbs"></script>

<div class="location">
	<a class="backlink" href="/dashboard"><i class="fa fa-chevron-left"></i> Dashboard</a><span class="slash">/</span> <span class="current">{{ Session::get("election")->title }}</span>
</div>

<div class="dashboard-section">
	<div class="section-title" style="margin-bottom:10px;">
		<span>Election Options</span>
	</div>

	<div class="section-description">
		Election options are the options shown to the voter when he/she visits the voting link
	</div>

	<div class="buttons-bar">
		<button id="add-options" class="waves-effect waves-dark button">
			<i class="fa fa-plus"></i> Add Option
		</button>
		<button id="save-options" class="waves-effect waves-dark button">
			<i class="fa fa-save"></i> Save Options
		</button>
	</div>

	<form id="mainform" action="/data/election/{{ Session::get("election")->id }}" method="POST" enctype="multipart/form-data">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<div id="options-grid-container" class="grid row">
			
		</div>
	</form>
</div>