@extends('layouts.dashboard')

@section('title', 'Dashboard | Manape.lk')

@section('content')

	<div class="dashboard-tabs">

		<a href="#elections">
			<div class="dashboard-tab active waves-effect waves-dark">
				<i class="fa fa-file"></i>
				<span>Elections</span>
			</div>
		</a>

		<a href="#votes">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-thumbs-up"></i>
				<span>Votes</span>
			</div>
		</a>

		<a href="#settings">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-gear"></i>
				<span>Settings</span>
			</div>
		</a>
	</div>	

	<div class="notifications card">

		<a href="#">
			<div class="waves-effect waves-dark notifications-clear-button">
				Clear Notifications
			</div>
		</a>

		@if (false)
			<a href="#">
				<div class="waves-effect waves-dark notification">
					<table>
						<tr>
							<td>
								<img class="notification-icon" src="resources/profile.jpg">
							</td>
							<td class="notification-text"><span class="notification-highlight">@ragnadole</span> invited you to &quot;Ninponix Invention Club Election&quot;</td>
						</tr>
					</table>
				</div>
			</a>
		@else
			<div class="caught">
				<img src="/resources/caught-up.png" height="75px">
				<p>All caught up!</p>
			</div>
		@endif

	</div>

	<div class="container dashboard-content">
		<div id="display">
			
		</div>
	</div>

@endsection


@section('scripts')

	<script type="text/javascript" src="/scripts/ccrouter/ccrouter.js"></script>
	<script type="text/javascript" src="/scripts/dashboard.js"></script>

@endsection

