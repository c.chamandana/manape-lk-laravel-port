@extends('layouts.dashboard')

@section('title',  Session::get('election')->title . ' | Manape.lk')

@section('content')

	<div class="dashboard-tabs">

		<a href="#overview">
			<div class="dashboard-tab active waves-effect waves-dark">
				<i class="fa fa-home"></i>
				<span>Overview</span>
			</div>
		</a>

		<a href="#analytics">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-line-chart"></i>
				<span>Analytics</span>
			</div>
		</a>

		<a href="#settings">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-gear"></i>
				<span>Settings</span>
			</div>
		</a>

		<a href="#options">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-list"></i>
				<span>Options</span>
			</div>
		</a>
		
		<a href="#launch">
			<div class="dashboard-tab waves-effect waves-dark">
				<i class="fa fa-rocket"></i>
				<span>Launch</span>
				<span class="launch-status {{ strtolower(Session::get("election")->status) }}"></span>
			</div>
		</a>
	</div>	

	<div class="notifications">

		<a href="#">
			<div class="waves-effect waves-dark notifications-clear-button">
				Clear Notifications
			</div>
		</a>

		<a href="#">
			<div class="waves-effect waves-dark notification">
				<table>
					<tr>
						<td>
							<img class="notification-icon" src="/resources/profile.jpg">
						</td>
						<td class="notification-text"><span class="notification-highlight">@ragnadole</span> invited you to &quot;Ninponix Invention Club Election&quot;</td>
					</tr>
				</table>
			</div>
		</a>

	</div>

	<div class="container dashboard-content">
		<div id="display">
			
		</div>
	</div>

@endsection


@section('scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>

	<script src="//cdn.quilljs.com/1.3.5/quill.js"></script>
	
	<script type="text/javascript" src="/scripts/ccrouter/ccrouter.js"></script>

	<script type="text/javascript">
		var startdate = "{{ Session::get('election')->startdate }}";
		var enddate = "{{ Session::get('election')->enddate }}";

		var format = "YYYY-MM-D HH:mm:s";
		var backFormat = "D MMMM, YYYY";
		var backFormatTime = "HH:mm";

		var start = moment(startdate, format);
		var end = moment(enddate, format);

		const __id = "{{ Session::get('election')->id }}";

		var _description = "{{ Session::get('election')->description }}";

		var _votesFor = JSON.parse("{{ json_encode(Session::get("categorized-votes")["votesFor"]) }}".replaceAll("&quot;", "\""));
		var _candidates = Object.keys(_votesFor);
		var _votesCount = Object.values(_votesFor);

		// Generate random colors
		function getRandomColor() 
		{
		    var letters = '0123456789ABCDEF'.split('');
		    var color = '#';
		    for (var i = 0; i < 6; i++ ) {
		        color += letters[Math.floor(Math.random() * 16)];
		    }
		    return color;
		}

		var _backgroundColors = [];

		for (var i = 0; i < _candidates.length; i ++)
		{
			_backgroundColors.push(getRandomColor()); // Put the random color in
		}

		var _month = JSON.parse("{{ json_encode(Session::get("categorized-votes")["month"]) }}".replaceAll("&quot;", "\""));
		var _daynumber = Object.keys(_month);
		var _dayvotes = Object.values(_month);

	</script>


	<script type="text/javascript" src="/scripts/election.js"></script>

@endsection

