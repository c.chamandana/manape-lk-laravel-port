@extends('layouts.thanks')

@section('title', 'Thank you for voting | Manape.lk')

@section('content')

<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">

	<div class="container center">
		<div class="card thanks-card z-depth-5 hoverable">
			<form>
				<span class="note"></span>

				<div class="thanks-bar">
					Thank you!
				</div>

				<div class="thanks-description">
					Your vote has been successfully submitted!
				</div>

				<a href="/dashboard">
					<div class="blue-button waves-effect waves-dark" style="display:inline-block;">
						GO TO DASHBOARD
					</div>
				</a>
			</form>
		</div>

		<div class="card link-card hoverable">
			<span style="color:#555;font-weight:bold;">Create your own election</span>

			<a href="/dashboard">
				<div class="blue-button waves-effect waves-dark" style="margin-left:15px;display:inline-block;">
					GET STARTED
				</div>
			</a>
		</div>
	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		
	</script>
@endsection

