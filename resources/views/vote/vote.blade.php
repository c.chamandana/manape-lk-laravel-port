@extends('layouts.dashboard')

@section('title',  Session::get("election")->title . ' | Manape.lk')

@section('content')

	<div id="votemodal" class="modal">
		<div class="modal-content">
			<span style="display:block;font-size:20px;font-weight:bold;">Voting confirmation</span>
			<p>Are you sure you want to vote <b id="candidate-name"></b> for <b>{{ Session::get("election")->title }}</b></p>
		</div>
		<div class="modal-footer">
			<a id="submitter" href="" class="modal-action modal-close waves-effect blue-text btn-flat">Yes</a>
			<a class="modal-action modal-close waves-effect gray-text btn-flat">No</a>
		</div>
	</div>

	<div class="container vote-container z-depth-1">
		<div class="election-info">
			<span class="election-name">{{ Session::get("election")->title }}</span>
			<span class="org-name">{{ Session::get("v-org")->name }}</span>
			<hr>
		</div>

		<div class="election-description">
			
		</div>

	</div>

	<div class="container" style="padding-bottom:50px;">
		<div class="dashboard-section">
			<div class="section-title">
				Select Candidate
			</div>
		</div>

		@for ($i = 0; $i < sizeof(Session::get("election-data")["options"]); $i++)
			<?php $option = Session::get("election-data")["options"][$i]; ?>
			<div data-index="{{ $i }}" class="modal-launch">
				@if (Session::has("user-vote"))
					@if (Session::get("user-vote")->vote == ($i + 1))
						<div class="voted-option card horizontal hoverable waves-effect waves-dark">
					@else 
						<div class="card horizontal hoverable waves-effect waves-dark">
					@endif
				@else
				<div class="card horizontal hoverable waves-effect waves-dark">
				@endif
					<div class="card-image">
						<img src="{{ $option["image"] }}" height="200">
					</div>
					<div class="card-stacked">
						<div class="card-content">
							<p class="option-title">{{ $option["title"] }}</p>
							<p class="option-description">{{ $option["bio"] }}</p>
						</div>
					</div>
				</div>
			</div>
		@endfor
	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		const _id = "{{ Session::get("election")->id }}";
		const _description = "{{ Session::get("election")->description }}";
		const _options = JSON.parse(
			("{{ json_encode(Session::get('election-data')['options']) }}").replaceAll("&quot;", "\"")
		);

		@if (Session::has("user-vote"))
			Materialize.toast('You have already voted in this election', 4000, "alert-error");
		@endif
	</script>
	<script type="text/javascript" src="/scripts/vote.js"></script>
@endsection

