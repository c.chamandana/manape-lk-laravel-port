@extends('layouts.public')

@section('title', 'Go green. Save Ceylon | Manape.lk')

@section('content')

<div class="gogreen-background" style="background:url('resources/go_green/background.png');">
	<div class="go-green-content">
		<table class="hide-on-small-only" style="width:auto !important;position:relative;left:50%;transform:translateX(-50%)">
			<tr>
				<td style="width:350px;text-align:right" data-aos="zoom-in">
					<img class="go-green-image" src="resources/uielems/travel_river_animation_ramotion.gif">
				</td>
				<td style="text-align:center;width:400px;">
					<span data-aos="fade-left" class="go-green-topic">Go <span class="gone-green-text">Green.</span> Save Ceylon.</span>
					<p class="quote" data-aos="fade-left" style="max-width:400px;font-weight:bold;">&quot;You must be the change you want to see in the world&quot;<br>
					<span class="quote-sayer" style="font-size:13px">- Mahatma Gandhi</span></p>
				</td>
			</tr>
		</table>

		<div class="hide-on-med-and-up">
			<div class="container">
				<img  data-aos="zoom-in" class="go-green-image" src="resources/uielems/travel_river_animation_ramotion.gif">

				<span data-aos="fade-left" class="go-green-topic">Go <span class="gone-green-text">Green.</span> Save Ceylon.</span>
					<p class="quote" data-aos="fade-left" style="max-width:400px;font-weight:bold;">&quot;You must be the change you want to see in the world&quot;<br>
					<span class="quote-sayer" style="font-size:13px">- Mahatma Gandhi</span></p>
			</div>
		</div>
	</div>

	<div class="container padded" style="margin-top:100px">
		<p class="center gray-subtext big">Let us run an eco-friendly election campaign </p>

		<p>It makes no sense to have hundreds of A4 size posters on a roadside only for the competitor to stick theirs over them. Join with us to run an eco-friendly election campaign for the upcoming Elections.
		</p>

		<p>You should be sensitive toward the environment during the election campaigns and support to refrain from using polythene and plastic decorations, flyers, banners, posters, flags etc., and to be mindful not to pollute the environment during the election campaign.</p>

		<p>You can conduct election campaigns through more modern and eco-friendly methods including the social networks, media, telephone, internet, email, face book. TV viewership is rising even in the countryside. The future of political campaigning, though, will be in social media.</p>

		<p>In fact, to capture the youthful voter, there will be few options away from social media. Which are much more cost effective and environment friendly. </p>
		
	</div>

	<div id="img-wrapper">
  	<button style="display:none" id="left-button" class="disabled"><<</button>
  	<button style="display:none;" id="right-button" class="disabled">>></button>
  	<img class="gallery-img" id="img-1" src="/resources/go_green/slider/1.jpg" alt="Image" />
  	<img class="gallery-img" id="img-2" src="/resources/go_green/slider/2.jpg" alt="Image" />
    <img class="gallery-img" id="img-3" src="/resources/go_green/slider/3.jpg" alt="Image" />
	<img class="gallery-img" id="img-4" src="/resources/go_green/slider/4.jpg" alt="Image" />
	<img class="gallery-img" id="img-5" src="/resources/go_green/slider/5.jpg" alt="Image" />
	<img class="gallery-img" id="img-6" src="/resources/go_green/slider/6.jpg" alt="Image" />
</div>
</div>




@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		$(".owl-carousel").owlCarousel({
			autoWidth : true
		});
		
		lightbox.option({
	      'resizeDuration': 200,
	      'wrapAround': true
	    });
	});

	$(document).ready(function() {
  var imgs = [];
  var imgCount = $('.gallery-img').length;
  var maxZIndex = Math.floor(imgCount / 2) + 1;
  var degreePerImage = 360 / imgCount;
  var galleryRadius = 1000;
  var imgSize = 680;
  var minImgSize = 100;
  var animating = false;
  var autoPlaying = true;
  var autoPlayID = null;

  function degToRad(degree) { return degree / 180 * Math.PI; }

  /* Initializing the image gallery view */
  for (var i = 1; i <= imgCount; i++) {
    var imgObj = {
      element: $('#img-' + i),
      angle: degreePerImage * (i - 1),
      zIndex: (maxZIndex - i + 1 > 0) ? (maxZIndex - i + 1) : (i - maxZIndex) 
    };
    imgObj.left = parseInt(imgObj.element.css('left')) + galleryRadius * Math.sin(degToRad(imgObj.angle));
    imgObj.opacity = Math.abs(imgObj.angle - 180) / 180;
    imgObj.size = (imgSize - minImgSize) * Math.abs(imgObj.angle - 180) / 180 + minImgSize;
    imgs.push(imgObj);
  }


  for (var j = 0; j < imgCount; j++) {
    var imgElement = imgs[j].element;
    imgElement.css({
      'left': imgs[j].left,
      'z-index': imgs[j].zIndex,
      'opacity': imgs[j].opacity,
      'width': imgs[j].size,
      'height': imgs[j].size / 16 * 9
    });
  }

  /* Setting Up Events */
  $('button').on('click', function(event) {
    event.preventDefault();
    if (animating) { return; } else animating = true;
    
    if (this.id === 'right-button') {
      var mem = imgs[0].element;
      for (var i = 1; i < imgCount; i++) {
        imgs[i - 1].element = imgs[i].element;
      }
      imgs[imgCount - 1].element = mem;
    } else if (this.id === 'left-button') {
      var mem = imgs[imgCount - 1].element;
      for (var i = imgCount - 1; i >= 1; i--) {
        imgs[i].element = imgs[i - 1].element;
      }
      imgs[0].element = mem;
    }

    /* Animate and assign next state */
    for (var j = 1; j <= imgCount; j++) {
      imgElement = imgs[j - 1].element;
      imgElement.animate({
        left: imgs[j - 1].left,
        opacity: imgs[j - 1].opacity,
        width: imgs[j - 1].size,
        height: imgs[j - 1].size / 16 * 9,
      }, 500, 'easeOutQuart', function() {
        if (animating) animating = false;
      });
    }
  });

  function autoPlay() {
    autoPlayID = setTimeout(function() {
      $('#right-button').click();
      if (autoPlaying)autoPlay();
    }, 2000);
  }

  $('#auto-play-trigger').on('click', function(event) {
    event.preventDefault();
    autoPlaying = !autoPlaying;
    if (autoPlaying) {
      $(this).text('Stop Auto Play');
      $('#left-button').addClass('disabled');
      $('#right-button').addClass('disabled');
      autoPlay();
    } else {
      window.clearTimeout(autoPlayID);
      $(this).text('Auto Play');
      $('#left-button').removeClass('disabled');
      $('#right-button').removeClass('disabled');
    }
  })
  autoPlay();
  
});
</script>

@endsection