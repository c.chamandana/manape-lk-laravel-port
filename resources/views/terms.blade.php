@extends('layouts.public')

@section('title', 'Terms and Conditions of Service | Manape.lk')

@section('content')

<div class="container" style="margin-bottom:150px">
	<div class="gray-subtext big">Manape.lk</div>
	<div class="blue-subtext">Terms and Conditions of Service</div>

	<div class="large-space" style="margin-bottom:30px;"></div>

	<div style="font-size:13px;text-align:justify">
		<h5 style="font-weight:bold">Terms of Service</h5>
		<p style="text-indent:50px;text-align:justify">
			Please read these Terms of Service (collectively with manape.lk Privacy Policy [<a href="http://www.manape.lk/privacy-policy">http://manape.lk/privacy-policy</a>], the “Terms of Service”) fully and carefully before using <a href="http://www.manape.lk">http://manape.lk/</a> (the “Site”) and the services, features, content or applications offered by Zeon Lab Digital Agency. (“we,” “us,” or “Zeon Lab”, “Manape.lk”) (Collectively with the Site, the “Services”). These Terms of Service set forth the legally binding terms and conditions for your use of the Site and the Services. 
		</p>

		<h5 style="font-weight:bold">1. Acceptance of Terms</h5>
		<p style="text-indent:50px;text-align:justify">
			<ol>
				<li>
					By registering for and/or using the Services in any manner, including but not limited to visiting or browsing the Site and using the Services to create and run an election, you agree to these Terms of Service and all other operating rules, policies and procedures that may be published from time to time on the Site by manape.lk, each of which is incorporated by reference and each of which may be updated from time to time without notice to you. 
				</li>
				<li>
					Certain of the Services may be subject to additional terms and conditions specified by manape.lk from time to time; your use of such Services is subject to those additional terms and conditions, which are incorporated into these Terms of Service by this reference. 
				</li>
				<li>
					These Terms of Service apply to all users of the Services, including, without limitation, users who are contributors of content, information, and other materials or services, registered or otherwise. 
				</li>
			</ol>
		</p>

		<h5 style="font-weight:bold">2. Modification</h5>
		<p style="text-indent:50px;text-align:justify">
			manape.lk reserves the right, at its sole discretion, to modify or replace any of these Terms of Service, or change, suspend, or discontinue the Services (including without limitation, the availability of any feature, database, or content) at any time by posting a notice on the Site or by sending you notice through the Services or by e-mail. Election Runner may also impose limits on certain features of the Services or restrict your access to parts or all of the Services without notice or liability. It is your responsibility to check these Terms of Service periodically for changes. Your use of the Services following the posting of any changes to these Terms of Service constitutes acceptance of those changes. 
		</p>

		<h5 style="font-weight:bold">3. Eligibility</h5>
		<p style="text-indent:50px;text-align:justify">
			manape.lk reserves the right, at its sole discretion, to modify or replace any of these Terms of Service, or change, suspend, or discontinue the Services (including without limitation, the availability of any feature, database, or content) at any time by posting a notice on the Site or by sending you notice through the Services or by e-mail. Election Runner may also impose limits on certain features of the Services or restrict your access to parts or all of the Services without notice or liability. It is your responsibility to check these Terms of Service periodically for changes. Your use of the Services following the posting of any changes to these Terms of Service constitutes acceptance of those changes. 
		</p>


		<h5 style="font-weight:bold">4. Registration</h5>
		<p style="text-indent:50px;text-align:justify">
			While some features of the Service are available to unregistered users, for broader access to the Services you must register with manape.lk (creating an “Account”). In order to register, you must provide an accurate e-mail address and select an organization name. You are responsible for updating the accuracy of the e-mail address that you provide to manape.lk to be associated with your Account. Regarding organization names, you shall not (1) select or use, as your username, the name of another person with the intent to impersonate that person; (2) select or use, as your organization name, a name subject to any rights of a person or entity other than you without appropriate authorization or (3) select or use, as your organization name, a name that is otherwise offensive, vulgar or obscene or (4) select or use multiple accounts for the same organization with intent to abuse the promotional free voters. (5) Create or launch multiple elections with direct or indirect intent to abuse the generous free voter promotion. 
		</p>

		<h5 style="font-weight:bold">5. Account Security</h5>
		<p style="text-align:justify">
			You are solely responsible for the activity that occurs on your Account, and for keeping your Account secure. You are not permitted to use another Account without permission. You must notify us immediately of any breach of security or other unauthorized use of your Account. You should never publish, distribute or post login information for your Account. 
		</p>


		<h5 style="font-weight:bold">6. Manape.lk Services</h5>
		<p style="text-align:justify">
			The Services allow you to either (i) create an election or (ii) or vote in an election as defined by the user that created the election. 
		</p>

		<h5 style="font-weight:bold">7. Content</h5>
		<p style="text-align:justify">
			The Services allow you to either (i) create an election or (ii) or vote in an election as defined by the user that created the election. 
		</p>

		<h6>1. Definition.</h6>
		<p style="text-align:justify">
			For purposes of these Terms of Service, the term “Content” includes, without limitation, election, ballot, candidates, voters, design, candidate photos, election results, videos, audio clips, written posts and comments, information, data, text, photographs, software, scripts, graphics, and interactive features generated, provided, or otherwise made accessible on or through the Services. 
		</p>

		<h6>2. User Content. </h6>

		<p style="text-align:justify">All Content added, created, uploaded, submitted, distributed, or posted to the Services by users, whether publicly posted or privately transmitted (collectively “User Content”), is the sole responsibility of the user who originated it. You acknowledge that all Content accessed by you using the Services is at your own risk and you will be solely responsible for any damage or loss to you or any other party resulting therefrom. When you delete your User Content, it will be removed from the Services. </p>
			
		<h6>3. Manape.lk Content. </h6>
		<p style="text-align:justify">The Services contain Content specifically provided by manape.lk or its partners and such Content is protected by copyrights, trademarks, service marks, patents, trade secrets or other proprietary rights and laws. You shall abide by and maintain all copyright notices, information, and restrictions contained in any Content accessed through the Services. </p>

		<h6>4. Use License. </h6>
		<p style="text-align:justify">Subject to these Terms of Service, manape.lk grants each user of the Services a worldwide, non-exclusive, non-sublicensable and non-transferable license to use the Content, solely for personal, non-commercial use as part of using the Services. Use, reproduction, modification, distribution or storage of any Content for other than personal, non-commercial use is expressly prohibited without prior written permission from manape.lk, or from the copyright holder identified in such Content’s copyright notice. If you would like to use the Services for commercial purposes, consider purchasing a license for our manape.lk Enterprise services, or contact us regarding other types of uses. </p>
		</p>



		<h6>5. License Grants. </h6>
		<b>1. License to manape.lk. </b>
		<p>By submitting User Content through the Services, you hereby do and shall grant manape.lk a srilankan, non-exclusive, royalty-free, fully free, sub licensable and transferable license to use, edit, modify, reproduce, distribute, prepare derivative works of, display, perform, and otherwise fully exploit the User Content in connection with the Site, the Services and Election Runner’s (and its successors and assigns’) business, including without limitation for promoting and redistributing part or all of the Site (and derivative works thereof) or the Services in any media formats and through any media channels (including, without limitation, third party websites and feeds). </p>

		<b>2. License to Users. </b>
		<p>You also hereby do and shall grant each user of the Site and/or the Services a non-exclusive license to access your User Content through the Site and the Services, and to use, edit, modify, reproduce, distribute, prepare derivative works of, display and perform such User Content. </p>

		<b>3. No Infringement. </b>
		<p>You represent and warrant that you have all rights to grant such licenses without infringement or violation of any third party rights, including without limitation, any privacy rights, publicity rights, copyrights, contract rights, or any other intellectual property or proprietary rights. </p>

		<h6>6. Availability of Content. </h6>
		<p style="text-indent:50px;text-align:justify">Manape.lk does not guarantee that any Content will be made available on the Site or through the Services. Further, manape.lk has no obligation to monitor the Site or the Services. However manape.lk reserves the right to (i) remove, edit or modify any Content in its sole discretion, at any time, without notice to you and for any reason (including, but not limited to, upon receipt of claims or allegations from third parties or authorities relating to such Content or if manape.lk is concerned that you may have violated these Terms of Service), or for no reason at all and (ii) remove or block any Content from the Services.</p>

		<p style="text-indent:50px;">manape.lk ‘s election builder and voting platform require Javascript to be enabled. manape.lk also utilizes "local storage" available in all major browsers to store information relevant to the election builder and voting platform. At this time we support the following browsers: Internet Explorer 10+, Safari 9+, Firefox 45+, Chrome 45+, iOS Safari 9+, Android Browser 4.4+, Opera 36+. manape.lk does not guarantee the website will function correctly in any browser or on any specific device. </p>
		
		<h5>8. Rules of Conduct. </h5>

		<ol>
			<li>
				You promise not to use the Services for any purpose that is prohibited by these Terms of Service. You are responsible for all of your activity in connection with the Services. 
			</li>
			<li>
				You shall not, and shall not permit any third party to, either (a) take any action or (b) upload, download, post, submit or otherwise distribute or facilitate distribution of any Content (including User Content) on or through the Service that: 

				<ol>
					<li>
						infringes any patent, trademark, trade secret, copyright, right of publicity or other right of any other person or entity or violates any law or contractual duty is unlawful, such as content that is threatening, abusive, harassing, defamatory, libelous, fraudulent, invasive of another’s privacy, or tortuous; 
					</li>
					<li>
						constitutes unauthorized or unsolicited advertising, junk or bulk e-mail (“spamming”); 
					</li>
					<li>
						contains software viruses or any other computer codes, files, or programs that are designed or intended to disrupt, damage, limit or interfere with the proper function of any software, hardware, or telecommunications equipment or to damage or obtain unauthorized access to any system, data, password or other information of manape.lk or any third party; 
					</li>
					<li>
						impersonates any person or entity, including any employee or representative of manape.lk; 
					</li>
					<li>includes anyone’s identification documents or sensitive financial information; or </li>
					<li>is otherwise determined by manape.lk to be inappropriate at its sole discretion. </li>	
				</ol>
			</li>
			<li>
					You shall not: (i) take any action that imposes or may impose (as determined by manape.lk in its sole discretion) an unreasonable or disproportionately large load on manape.lk ‘s (or its third party providers’) infrastructure; (ii) interfere or attempt to interfere with the proper working of the Services or any activities conducted on the Services; (iii) bypass any measures manape.lk may use to prevent or restrict access to the Services (or other accounts, computer systems or networks connected to the Services); (iv) run any form of auto-responder or “spam” on the Services; (v) use manual or automated software, devices, or other processes to “crawl” or “spider” any page of the Site; (vi) harvest or scrape any Content from the Services; or (vii) otherwise take any action in violation of manape.lk’s guidelines and policies. 
			</li>
			<li>
				You shall not (directly or indirectly): (i) decipher, decompile, disassemble, reverse engineer or otherwise attempt to derive any source code or underlying ideas or algorithms of any aspect, feature or part of the Services, except to the limited extent applicable laws specifically prohibit such restriction; (ii) modify, translate, or otherwise create derivative works of any part of the Services; or (iii) copy, rent, lease, distribute, or otherwise transfer any of the rights that you receive hereunder. You shall abide by all applicable local, state, national and international laws and regulations. 
			</li>
			<li>
				manape.lk also reserves the right to access, read, preserve, and disclose any information as it reasonably believes is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request; (ii) enforce these Terms of Service, including investigation of potential violations hereof; (iii) detect, prevent, or otherwise address fraud, security or technical issues; (iv) respond to user support requests; or (v) protect the rights, property or safety of Election Runner, its users and the public. This includes exchanging information with other companies and organizations for fraud protection and spam prevention. 
			</li>
		</ol>
	
		<h5>9. Third Party Services.</h5>

		<p>The Services may permit you to link to other websites, services or resources on the Internet, such as Twitter and Facebook, and other websites, services or resources may contain links to the Services. When you access third party resources on the Internet, you do so at your own risk. These other resources are not under the control of Election Runner, and you acknowledge that manape.lk is not responsible or liable for the content, functions, accuracy, legality, appropriateness or any other aspect of such websites or resources. The inclusion of any such link does not imply endorsement by manape.lk  or any association with its operators. You further acknowledge and agree that manape.lk  shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such Content, goods or services available on or through any such website or resource. </p>


		<h5>10. Termination. </h5>
		<p>Manape.lk may terminate your access to all or any part of the Services at any time, with or without cause, with or without notice, effective immediately, which may result in the forfeiture and destruction of all information associated with your Account. If you wish to terminate your Account, you may do so by following the instructions on the Site. All provisions of these Terms of Service which by their nature should survive termination shall survive termination, including without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability. </p>

		<h5>11. Warranty Disclaimer. </h5>
		<p>Manape.lk  has no special relationship with or fiduciary duty to you. You acknowledge that Manape.lk has no control over, and no duty to take any action regarding: </p>

		<ol>
			<li>which users gains access to the Services;</li>
			<li>what Content you access via the Services; </li>
			<li>what effects the Content may have on you; </li>
			<li>how you may interpret or use the Content; or </li>
			<li>what actions you may take as a result of having been exposed to the Content.
				<ol>
					<li>
						You release Manape.lk from all liability for you having acquired or not acquired Content through the Services. The Services may contain, or direct you to websites containing, information that some people may find offensive or inappropriate. Manape.lk makes no representations concerning any Content contained in or accessed through the Services, and it will not be responsible or liable for the accuracy, copyright compliance, legality or decency of material contained in or accessed through the Services. 
					</li>
					<li>
						THE SERVICES AND CONTENT ARE PROVIDED “AS IS”, “AS AVAILABLE” AND WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTIES IMPLIED BY ANY COURSE OF PERFORMANCE OR USAGE OF TRADE, ALL OF WHICH ARE EXPRESSLY DISCLAIMED. ELECTION RUNNER, AND ITS DIRECTORS, EMPLOYEES, AGENTS, SUPPLIERS, PARTNERS AND CONTENT PROVIDERS DO NOT WARRANT THAT: (I) THE SERVICES WILL BE SECURE OR AVAILABLE AT ANY PARTICULAR TIME OR LOCATION; (II) ANY DEFECTS OR ERRORS WILL BE CORRECTED; (III) ANY CONTENT OR SOFTWARE AVAILABLE AT OR THROUGH THE SERVICES IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; OR (IV) THE RESULTS OF USING THE SERVICES WILL MEET YOUR REQUIREMENTS. YOUR USE OF THE SERVICES IS SOLELY AT YOUR OWN RISK. SOME STATES DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTIES, SO THE FOREGOING LIMITATIONS MAY NOT APPLY TO YOU. 
					</li>
					<li>
						ELECTRONIC COMMUNICATIONS PRIVACY ACT NOTICE (18 USC 2701-2711): ELECTION RUNNER MAKES NO GUARANTY OF CONFIDENTIALITY OR PRIVACY OF ANY COMMUNICATION OR INFORMATION TRANSMITTED ON THE SERVICES OR ANY WEBSITE LINKED TO THE SERVICES. Manape.lk will not be liable for the privacy of e-mail addresses, registration and identification information, disk space, communications, confidential or trade-secret information, or any other Content stored on Manape.lk’s equipment, transmitted over networks accessed by the Services, or otherwise connected with your use of the Services. 
					</li>
				</ol>
			</li>
		</ol>
		
		<h5>12. Indemnification. </h5>
		<p>You shall defend, indemnify, and hold harmless Manape.lk, its affiliates and each of their respective employees, contractors, directors, suppliers and representatives from all liabilities, claims, and expenses, including reasonable attorneys’ fees, that arise from or relate to your use or misuse of, or access to, the Site, Services, Content, or which otherwise arise from your User Content, violation of these Terms of Service, or infringement by you, or any third party using your Account, of any intellectual property or other right of any person or entity. Manape.lk reserves the right to assume the exclusive defense and control of any matter subject to indemnification by you, in which event you will assist and cooperate with Manape.lk in asserting any available defenses. </p>

		<h5>13. Limitation of Liability. </h5>
		<p>IN NO EVENT SHALL MANAPE.LK, ITS AFFILIATES NOR ANY OF THEIR RESPECTIVE DIRECTORS, EMPLOYEES, CONTRACTORS, AGENTS, PARTNERS, SUPPLIERS, REPRESENTATIVES OR CONTENT PROVIDERS, BE LIABLE UNDER CONTRACT, TORT, STRICT LIABILITY, NEGLIGENCE OR ANY OTHER LEGAL OR EQUITABLE THEORY WITH RESPECT TO THE SERVICES (I) FOR ANY LOST PROFITS, DATA LOSS, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR SPECIAL, DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF ANY KIND WHATSOEVER, SUBSTITUTE GOODS OR SERVICES (HOWEVER ARISING); OR (II) FOR ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE (REGARDLESS OF THE SOURCE OF ORIGINATION). NOTWITHSTANDING THE FOREGOING, UNDER NO CIRCUMSTANCES SHALL SUCH LIABILITY EXCEED ANY DAMAGES IN EXCESS OF ONE HUNDRED U.S. DOLLARS ($100.00) IN THE AGGREGATE. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS AND EXCLUSIONS MAY NOT APPLY TO YOU. </p>


		<h5>14. Entire Agreement and Severability. </h5>
		<p>These Terms of Service are the entire agreement between you and Manape.lk with respect to the Services and use of the Site, and supersede all prior or contemporaneous communications and proposals (whether oral, written or electronic) between you and Manape.lk with respect to the Site. If any provision of these Terms of Service is found to be unenforceable or invalid, that provision will be limited or eliminated to the minimum extent necessary so that these Terms of Service will otherwise remain in full force and effect and enforceable. </p>

		<h5>16. Miscellaneous.</h5> 
		<ol>
			<li>
				Force Majeure. <br>
				Manape.lk shall not be liable for any failure to perform its obligations hereunder where such failure results from any cause beyond Manape.lk’s reasonable control, including, without limitation, mechanical, electronic or communications failure or degradation. 
			</li>
			<li>
				Assignment. <br>
				These Terms of Service are personal to you, and are not assignable, transferable or sublicensable by you except with Manape.lk’s prior written consent. Manape.lk may assign, transfer or delegate any of its rights and obligations hereunder without consent. 
			</li>
			<li>
				Agency. <br>
				No agency, partnership, joint venture, or employment relationship is created as a result of these Terms of Service and neither party has any authority of any kind to bind the other in any respect. 
			</li>
			<li>
				Notices. <br>
				Unless otherwise specified in these Term of Service, all notices under these Terms of Service will be in writing and will be deemed to have been duly given when received, if personally delivered or sent by certified or registered mail, return receipt requested; when receipt is electronically confirmed, if transmitted by facsimile or e-mail; or the day after it is sent, if sent for next day delivery by recognized overnight delivery service. 
			</li>
			<li>
				No Waiver. <br>
				The failure of Manape.lk to enforce any part of these Terms of Service shall not constitute a waiver of its right to later enforce that or any other part of these Terms of Service. Waiver of compliance in any particular instance, does not mean that we will do so in the future. In order for any waiver of compliance with these Terms of Service to be binding, Manape.lk must provide you with written notice of such waiver, provided by one of its authorized representatives. 
			</li>
			<li>
				Headings. <br>
				The section and paragraph headings in these Terms of Service are for convenience only and shall not affect their interpretation. 
			</li>
		</ol>
		
		<h5>17. Contact. </h5>
<p>		You may contact <a href="http://manape.lk/">Manape.lk</a>: by mail 148, Gollummahara, Delgoda,11700, Sri Lanka; by e-mail at <a href="mailto:support@manape.lk">support@manape.lk</a>
	<br><br>
	<b>	Effective Date of Terms of Service: Jan 08, 2018. </b>

	</div>
</div>

@endsection