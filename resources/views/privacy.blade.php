@extends('layouts.public')

@section('title', 'Privacy Policy | Manape.lk')

@section('content')

<div class="container" style="margin-bottom:150px">
	<div class="gray-subtext big">Manape.lk</div>
	<div class="blue-subtext">Privacy Policy</div>

	<div class="large-space" style="margin-bottom:30px;"></div>

	<div style="font-size:13px;text-align:justify">
		
		<p style="text-indent:50px">
			At Manape.lk, we collect and manage user data according to the following Privacy Policy, with the goal of incorporating our company values: transparency, accessibility, sanity, usability. This document is part of Manape.lk’s Terms of Service, and by using Manape.lk (the “Website”), you agree to the terms of this Privacy Policy and the Terms of Service. Please read the Terms of Service in their entirety, and refer to those for definitions and contacts.
		</p>

		<h5>
			Data Collected
		</h5>

		<p style="text-indent:50px">
			We collect anonymous data from every visitor of the Website to monitor traffic and fix bugs. For example, we collect information like web requests, the data sent in response to such requests, the Internet Protocol address, the browser type, the browser language, and a timestamp for the request.
		</p>
		<p style="text-indent:50px">
			We ask you to log in and provide certain personal information (such as your name and email address) in order to be able to save your profile and the documents and comments associated with it. In order to enable these or any other login based features, we use cookies to store session information for your convenience. You can block or delete cookies and still be able to use Manape.lk, although if you do you will then be asked for your username and password every time you log in to the Website. In order to take advantage of certain features of the Website, you may also choose to provide us with other personal information, such as your picture or personal website, but your decision to utilize these features and provide such data will always be voluntary.
		</p>
		<p>
			You are able to view, change and remove your data associated with your profile. Should you choose to delete your account, please contact us at <a href="mailto:support@manape.lk">support@manape.lk</a> and we will follow up with such request as soon as possible.
		</p>
		<p>
			Minors and children should not use Manape.lk. By using the Website, you represent that you have the legal capacity to enter into a binding agreement.
		</p>


		<h5>Use of the Data</h5>

		<p>We only use your personal information to provide you the Manape.lk services or to communicate with you about the services or the Website.</p>
		<p>
		With respect to any documents you may choose to upload to Manape.lk, we take the privacy and confidentiality of such documents seriously. If you choose to make an election and its contents public, we recommend you redact any and all references to people and addresses, as we can't protect public data and we are not responsible for any violation of privacy law you may be liable for.</p>
		<p>We employ industry standard techniques to protect against unauthorized access of data about you that we store, including personal information.</p>
		
		<p>We do not share personal information you have provided to us without your consent, unless:</p>
		<ol type="disk">
			<li>doing so is appropriate to carry out your own request;</li>
			<li>we believe it's needed to enforce our Terms of Service, or that is legally required;</li>
			<li>we believe it's needed to detect, prevent or address fraud, security or technical issues; </li>
			<li>otherwise protect our property, legal rights, or that of others.</li>
		</ol>
		
		<p>Manape.lk is operated from the Sri Lanka by Zeon Lab digital agency. If you are visiting the Website from outside the S.L., you agree to any processing of any personal information you provide us according to this policy.</p>

		<p style="text-indent:50px">Manape.lk may contact you, by email or other means. For example, Manape.lk may send you promotional emails relating Manape.lk or other third parties Manape.lk feels you would be interested in, or communicate with you about your use of the Manape.lk website. Manape.lk may also use technology to alert us via a confirmation email when you open an email from us. If you do not want to receive email from Manape.lk, please opt out of receiving emails at the bottom of any Manape.lk emails or by editing your profile preferences.</p>

		<h5>Sharing of Data</h5>
		<p style="text-indent:50px;">We don't share your personal information with third parties. Only aggregated, anonymized data is periodically transmitted to external services to help us improve the Manape.lk Website and service. We may use anonymized ballot data to improve our service and publicly share information related to this anonymized data. We currently use Google Analytics (traffic analysis, SEO optimization), Mailchimp (mailing list management), Zendesk (email support). We listed below what data these third parties extract exactly. Feel free to check out their own Privacy Policies to find out more.</p>

		<ol>
			<li>
				Google Analytics: anonymous (ad serving domains, browser type, demographics, language settings, page views, time/date), pseudonymous (IP address) 
			</li>
			<li>
				Google Analytics: anonymous (ad serving domains, browser type, demographics, language settings, page views, time/date), pseudonymous (IP address) 
			</li>
			<li>
				Mailchimp: name and email of our beta subscription list
			</li>
			<li>Zendesk: anonymous</li>
		</ol>

		<p>We also use social buttons provided by services like Twitter, Google+, LinkedIn and Facebook. Your use of these third party services is entirely optional. We are not responsible for the privacy policies and/or practices of these third party services, and you are responsible for reading and understanding those third party services’ privacy policies.</p>

		<p>We employ and contract with people and other entities that perform certain tasks on our behalf and who are under our control (our “Agents”). We may need to share personal information with our Agents in order to provide products or services to you. Unless we tell you differently, our Agents do not have any right to use Personal Information or other information we share with them beyond what is necessary to assist us. You hereby consent to our sharing of Personal Information with our Agents.</p>

		<p>
		We may choose to buy or sell assets. In these types of transactions, user information is typically one of the transferred business assets. Moreover, if we, or substantially all of our assets, were acquired, or if we go out of business or enter bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. You acknowledge that such transfers may occur, and that any acquirer of us or our assets may continue to use your personal information as set forth in this policy.</p>

		<h5>Changes to the Privacy Policy</h5>

		<p>We may amend this Privacy Policy from time to time. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used. If we make major changes in the way we collect or use information, we will notify you by posting an announcement on the Website or sending you an email. In any case, you can track all changes via the TOS Tracker. A user is bound by any changes to the Privacy Policy when he or she uses the Services after such changes have been first posted.
		Should you have any question or concern, please write to <a href="mailto:support@manape.lk">support@manape.lk</a></p>



	</div>
</div>

@endsection