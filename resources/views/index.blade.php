<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Manape.lk | Your elections. Any device. Any location</title>
	<meta name="theme-color" content="#216ddd" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="resources/favicon.png">
	<link rel="stylesheet" type="text/css" href="bower_components/materialize/dist/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/font-awesome/css/font-awesome.min.css"> 
	<link rel="stylesheet" type="text/css" href="bower_components/aos/dist/aos.css">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Poppins" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>

<body>

	@include('partials.preloader')

	<span class="arrows scroll-btn">
		<a href="#">
			<span class="mouse"><span></span></span>
		</a>

		<p style="margin-left:-55px">Scroll me</p>	
	</span>

	<div class="scroll-darkener"></div>


	@include('partials.header')
	@include('partials.features')
	@include('partials.gogreen')
	@include('partials.contact')
	@include('partials.footer')
	

	<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="bower_components/materialize/dist/js/materialize.min.js"></script>
	<script type="text/javascript" src="bower_components/aos/dist/aos.js"></script>

	<script type="text/javascript" src="scripts/index.js"></script>
	<script type="text/javascript" src="scripts/common.js"></script>
</body>
</html>